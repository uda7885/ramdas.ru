<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();

function upload($arrFiles, $request)
{
    if(is_uploaded_file($arrFiles["videoFile"]["tmp_name"]))
    {
        move_uploaded_file($arrFiles["videoFile"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"]."/upload/tmp/".$arrFiles["videoFile"]["name"]);
        $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/upload/tmp/".$arrFiles["videoFile"]["name"]);
        $arFile["MODULE_ID"] = "video";
        $fid = CFile::SaveFile($arFile, "video");

        if (intval($fid)>0)
        {
            $arVideo = CFile::MakeFileArray($fid);
            $arProp = [
                "VIDEO" => $arVideo,
                "USER_ID" => Array( "VALUE" => $request->getPost("userID") ),
                "TYPE" => Array( "VALUE" => $request->getPost("videoType") ),
                "CATEGORY" => Array( "VALUE" => $request->getPost("videoRazd") ),
            ];
            $arFields = Array(
                "IBLOCK_ID" => 4,
                "NAME" => $request->getPost("videoTitle"),
                "ACTIVE" => "Y",
                "PROPERTY_VALUES" => $arProp
            );

            IB::addElem($arFields);


            CFile::Delete($fid);
            unlink($_SERVER["DOCUMENT_ROOT"]."/upload/tmp/".$arrFiles["videoFile"]["name"]);
        }

    } else {
        return 0;
    }
}

upload($_FILES, $request);
?>

<div>
    Файл загружен
</div>
