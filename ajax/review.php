<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();

$arProp = [
    "USER_ID" => Array( "VALUE" => $request->getPost("userID") )
];
$arFields = Array(
    "IBLOCK_ID" => 5,
    "NAME" => "Новый отзыв",
    "ACTIVE" => "Y",
    "DETAIL_TEXT" => $request->getPost("text"),
    "PROPERTY_VALUES" => $arProp
);

echo IB::addElem($arFields);