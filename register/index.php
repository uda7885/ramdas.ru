<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");

if($USER->GetLogin()){ LocalRedirect("/"); }

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();

if (count($request) > 0 ) {

	$dataReg = [
		"user_login" => $request->getPost("name"),
		"user_pass" => $request->getPost("password"),
		"conf_pass" => $request->getPost("conf_pass"),
		"user_email" => $request->getPost("email"),
		"user_date" => $request->getPost("date"),
		"recaptcha" => $request->getPost("g-recaptcha-response"),
	];

	$user = new checkAuth();
	$user->register($dataReg);
}
?>

<?$APPLICATION->IncludeComponent(
	"main.register",
	"",
	Array(
		"AUTH" => "Y",
		"REQUIRED_FIELDS" => array(),
		"SET_TITLE" => "Y",
		"SHOW_FIELDS" => array(),
		"SUCCESS_PAGE" => "",
		"USER_PROPERTY" => array(),
		"USER_PROPERTY_NAME" => "",
		"USE_BACKURL" => "Y",
		"ERRORS_STATUS" => (isset($user)) ? $user->mesStatus : "",
		"ERRORS_MSG" => (isset($user)) ? $user->message() : "",
		"DATA" => (isset($dataReg)) ? $dataReg : "",
	)
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>