<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

class Videos {

    public $IBLOCK_ID = 4;
    public $user;
    const FREE = 18;
    const SUBSCRIBE = 19;
    const PREMIUM = 20;
    const USER_SUBSCRIBE = 8;
    const USER_PREMIUM = 9;

    public function __construct()
    {
        Loader::includeModule("iblock");
        $this->user = new Users();
    }

    public function types()
    {
        $property_enums = CIBlockPropertyEnum::GetList(array("ID"=>"ASC"), array("IBLOCK_ID"=>$this->IBLOCK_ID, "CODE"=>"TYPE"));
        while($enum_fields = $property_enums->GetNext())
        {
            $arrTypes[$enum_fields["ID"]] = $enum_fields["VALUE"];
        }

        return $arrTypes;
    }

    public function razd()
    {
        $property_enums = CIBlockPropertyEnum::GetList(array("ID"=>"ASC"),
            array("IBLOCK_ID"=>$this->IBLOCK_ID, "CODE"=>"CATEGORY"));
        while($enum_fields = $property_enums->GetNext())
        {
            $arrCategory[$enum_fields["ID"]] = $enum_fields["VALUE"];
        }

        return $arrCategory;
    }

    public function razdID($ID)
    {
        $property_enums = CIBlockPropertyEnum::GetList(array("ID"=>"ASC"),
            array("IBLOCK_ID"=>$this->IBLOCK_ID, "CODE"=>"CATEGORY", "XML_ID"=>$ID ));
        while($enum_fields = $property_enums->GetNext())
        {
            return $enum_fields["ID"];
        }
    }

    public function razdName($ID)
    {
        $property_enums = CIBlockPropertyEnum::GetList(array("ID"=>"ASC"),
            array("IBLOCK_ID"=>$this->IBLOCK_ID, "CODE"=>"CATEGORY", "XML_ID"=>$ID ));
        while($enum_fields = $property_enums->GetNext())
        {
            return $enum_fields["VALUE"];
        }
    }

    public function getVideo($ID)
    {
        $arSelect = array("ID","NAME", "PROPERTY_VIDEO","PROPERTY_CATEGORY", "PROPERTY_TYPE", "CREATED_BY", "DATE_CREATE", "PROPERTY_VIEWS");
        $arFilter = array("IBLOCK_ID"=>$this->IBLOCK_ID, "ACTIVE"=>"Y", "ID"=>$ID);
        $res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);

        while($obj = $res->GetNextElement())
        {
            $properties[] = $obj->GetFields();
        }

        foreach ($properties as $key=>$item)
        {
            $properties[$key]["PROPERTY_VIDEO_PATH"] = CFile::GetPath($item["PROPERTY_VIDEO_VALUE"]);
            $properties[$key]["ACCEPT"] = $this->user->accept($item["PROPERTY_TYPE_ENUM_ID"]);
            $properties[$key]["ACCEPT_IMG"] = $this->acceptImg($item["PROPERTY_TYPE_ENUM_ID"]);
            $properties[$key]["LINK"] = $this->link($item["PROPERTY_TYPE_ENUM_ID"]);
        }

        return $properties[0];
    }

    public function listVideoRazd($ID)
    {
        if(empty($ID)) return;
        
        $arSelect = array("ID","NAME", "PROPERTY_VIDEO","PROPERTY_CATEGORY", "PROPERTY_TYPE", "PROPERTY_USER_ID");
        $arFilter = array("IBLOCK_ID"=>$this->IBLOCK_ID, "ACTIVE"=>"Y", array("PROPERTY"=>array("CATEGORY"=>$ID)));
        $res = CIBlockElement::GetList(array("ID"=>"DESC"), $arFilter, false, array(), $arSelect);
        
        while($obj = $res->GetNextElement())
        {
            $properties[] = $obj->GetFields();
        }

        foreach ($properties as $key=>$item)
        {
            $properties[$key]["PROPERTY_VIDEO_PATH"] = CFile::GetPath($item["PROPERTY_VIDEO_VALUE"]);
            $properties[$key]["ACCEPT"] = $this->user->accept($item["PROPERTY_TYPE_ENUM_ID"]);
            $properties[$key]["ACCEPT_IMG"] = $this->acceptImg($item["PROPERTY_TYPE_ENUM_ID"]);
            $properties[$key]["LINK"] = $this->link($item["PROPERTY_TYPE_ENUM_ID"]);
        }

        return $properties;
    }

    public function listVideoTrener($ID)
    {
        $arSelect = array("ID","NAME", "PROPERTY_VIDEO", "PROPERTY_TYPE");
        $arFilter = array("IBLOCK_ID"=>$this->IBLOCK_ID, "ACTIVE"=>"Y", "CREATED_BY"=>$ID );
        $res = CIBlockElement::GetList(array("ID"=>"DESC"), $arFilter, false, array(), $arSelect);

        while($obj = $res->GetNextElement())
        {
            $properties[] = $obj->GetFields();
        }

        foreach ($properties as $key=>$item)
        {
            $properties[$key]["PROPERTY_VIDEO_PATH"] = CFile::GetPath($item["PROPERTY_VIDEO_VALUE"]);
            $properties[$key]["ACCEPT"] = $this->user->accept($item["PROPERTY_TYPE_ENUM_ID"]);
            $properties[$key]["ACCEPT_IMG"] = $this->acceptImg($item["PROPERTY_TYPE_ENUM_ID"]);
            $properties[$key]["LINK"] = $this->link($item["PROPERTY_TYPE_ENUM_ID"]);
        }

        return $properties;
    }

    public function getAllVideo()
    {
        $arSelect = Array("ID","NAME", "PROPERTY_VIDEO", "PROPERTY_TYPE", "PROPERTY_USER_ID");
        $arFilter = Array("IBLOCK_ID"=>$this->IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, false, false, $arSelect);

        while($ob = $res->GetNextElement())
        {
            $properties[] = $ob->GetFields();
        }

        foreach ($properties as $key=>$item)
        {
            $properties[$key]["PROPERTY_VIDEO_PATH"] = CFile::GetPath($item["PROPERTY_VIDEO_VALUE"]);
            $properties[$key]["ACCEPT"] = $this->user->accept($item["PROPERTY_TYPE_ENUM_ID"]);
            $properties[$key]["ACCEPT_IMG"] = $this->acceptImg($item["PROPERTY_TYPE_ENUM_ID"]);
            $properties[$key]["LINK"] = $this->link($item["PROPERTY_TYPE_ENUM_ID"]);
        }

        return $properties;
    }

    public function searchVideo($q)
    {
        $q = strtolower($q);
        $arSelect = Array("ID","NAME", "PROPERTY_VIDEO", "PROPERTY_TYPE", "PROPERTY_USER_ID");
        $arFilter = Array("IBLOCK_ID"=>$this->IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "NAME"=>"%$q%");
        $res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, false, false, $arSelect);

        while($ob = $res->GetNextElement())
        {
            $properties[] = $ob->GetFields();
        }

        foreach ($properties as $key=>$item)
        {
            $properties[$key]["PROPERTY_VIDEO_PATH"] = CFile::GetPath($item["PROPERTY_VIDEO_VALUE"]);
            $properties[$key]["ACCEPT"] = $this->user->accept($item["PROPERTY_TYPE_ENUM_ID"]);
            $properties[$key]["ACCEPT_IMG"] = $this->acceptImg($item["PROPERTY_TYPE_ENUM_ID"]);
            $properties[$key]["LINK"] = $this->link($item["PROPERTY_TYPE_ENUM_ID"]);
        }

        return $properties;
    }

    public function getFreeVideo()
    {
        $arSelect = Array("ID","NAME", "PROPERTY_VIDEO", "PROPERTY_TYPE", "PROPERTY_USER_ID");
        $arFilter = Array("IBLOCK_ID"=>$this->IBLOCK_ID, "ACTIVE"=>"Y", array("PROPERTY"=>array("TYPE"=>self::FREE)));
        $res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, false, Array ("nTopCount" => 3), $arSelect);

        while($ob = $res->GetNextElement())
        {
            $properties[] = $ob->GetFields();
        }

        foreach ($properties as $key=>$item)
        {
            $properties[$key]["PROPERTY_VIDEO_PATH"] = CFile::GetPath($item["PROPERTY_VIDEO_VALUE"]);
        }

        return $properties;
    }

    public function acceptImg($type)
    {
        switch ($type)
        {
            case self::SUBSCRIBE:
                return "videoSubscribe.png";
            case self::PREMIUM:
                return "videoPremium.png";
        }
    }

    public function link($type)
    {
        switch ($type)
        {
            case self::SUBSCRIBE:
                return "/subscribe/";
            case self::PREMIUM:
                return "/premium/";
        }
    }

    public function getClass($type)
    {
        switch ($type)
        {
            case self::FREE:
                return "free";
            case self::SUBSCRIBE:
                return "subscribe";
            case self::PREMIUM:
                return "premium";
        }
    }

    public function typesHB()
    {
        Loader::includeModule("highloadblock");

        $hlbl = 4;
        $hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();

        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $rsData = $entity_data_class::getList(array(
            "select" => array("*"),
            "order" => array("ID" => "ASC"),
            "filter" => array()
        ));

        while($arData = $rsData->Fetch()){
            $types[$arData["ID"]] = $arData["UF_NAME"];
        }

        return $types;
    }

    public function categoriesHB()
    {
        Loader::includeModule("highloadblock");

        $hlbl = 5;
        $hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();

        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $rsData = $entity_data_class::getList(array(
            "select" => array("*"),
            "order" => array("ID" => "ASC"),
            "filter" => array()
        ));

        while($arData = $rsData->Fetch()){
            $category[$arData["ID"]] = $arData["UF_NAME"];
        }

        return $category;
    }
}