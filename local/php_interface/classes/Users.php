<?php

Class Users {

    public $imgProfile;
    public $errors = [];
    public $msg = [];
    const FREE = 18;
    const SUBSCRIBE = 19;
    const PREMIUM = 20;
    const USER_SUBSCRIBE = 8;
    const USER_PREMIUM = 9;
    const USER_COACH = 10;

    public function getID()
    {
        global $USER;
        return $USER->GetID();
    }

    public function getByID($ID)
    {
        $result = \Bitrix\Main\UserTable::getList(array(
            "select" => array("ID", "NAME", "LAST_NAME", "EMAIL", "PERSONAL_CITY", "PERSONAL_PHONE", "PERSONAL_GENDER", "PERSONAL_BIRTHDAY",
                "PERSONAL_PHOTO", "PERSONAL_NOTES", "PERSONAL_GENDER", "UF_INSTA" ),
//            "select" => array("*"),
            "order" => array("ID"=>"ASC"),
            "filter" => array("ID"=>$ID),
            "limit" => 1
        ));

        while ($arUser = $result->fetch()) {
            return $arUser;
        }
    }

    public function getGroup()
    {
        return CUser::GetUserGroup($this->getID());
    }

    public function accept($ID)
    {
        if(self::FREE == $ID) return true;

        if(self::SUBSCRIBE == $ID && in_array(self::USER_SUBSCRIBE, $this->getGroup() )) {
            return true;
        }

        if(self::PREMIUM == $ID && in_array(self::USER_PREMIUM, $this->getGroup() )) {
            return true;
        }

        return false;
    }

    public function getName($ID)
    {
        $result = \Bitrix\Main\UserTable::getList(array(
            "select" => array("ID", "NAME"),
            "order" => array("ID"=>"ASC"),
            "filter" => array("ID"=>$ID),
            "limit" => 1
        ));

        while ($arUser = $result->fetch()) {
            return $arUser["NAME"];
        }
    }

    public function restore($email)
    {
        $result = \Bitrix\Main\UserTable::getList(array(
            "select" => array("ID"),
            "order" => array("ID"=>"ASC"),
            "filter" => array("EMAIL"=>$email, "UF_REG" => "Y"),
            "limit" => 1
        ));

        if ($arUser = $result->fetch()) {
             $userID = $arUser["ID"];
        } else {
            array_push($this->errors, "Пользователь с таким email не найден.");
            return;
        }

        $newPass = $this->random(8);
        $user = new CUser;
        $fields = Array(
            "PASSWORD" => $newPass,
            "CONFIRM_PASSWORD" => $newPass
        );
        $res = $user->Update($userID, $fields);

        if($res){
            
            array_push($this->msg, "Ваш новый пароль был выслан на эл. почту");

            $fields = [
                "THEME" => "Восстановление пароля на сайте Ramdas.ru",
                "PASSWORD" => $newPass,
                "RESTORE_EMAIL" => $email,
            ];

            CEvent::Send("restore", S1, $fields);

            return true;
        } else {
            array_push($this->errors, $user->LAST_ERROR);
        }
        
    }

    function random($n) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    public function getNotes($ID)
    {
        $result = \Bitrix\Main\UserTable::getList(array(
            "select" => array("ID", "PERSONAL_NOTES"),
            "order" => array("ID"=>"ASC"),
            "filter" => array("ID"=>$ID),
            "limit" => 1
        ));

        while ($arUser = $result->fetch()) {
            return $arUser["PERSONAL_NOTES"];
        }
    }

    public function getGroupByID($ID)
    {
        return CUser::GetUserGroup($ID);
    }

    public function isCoach($ID)
    {
        return in_array(self::USER_COACH, $this->getGroupByID($ID));
    }


    public function addUser($arFields)
    {
        $user = new CUser;
        $ID = $user->Add($arFields);
        if (intval($ID) > 0){
            return $ID;
        } else{
            return $user->LAST_ERROR;
        }
    }

    public function updUser($ID, $arFields)
    {
        $user = new CUser;
        return $user->Update($ID, $arFields);
    }

    public function addImgProfile($arrFiles)
    {
        if(is_uploaded_file($arrFiles["photo"]["tmp_name"]))
        {
            move_uploaded_file($arrFiles["photo"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"]."/upload/tmp/".$arrFiles["photo"]["name"]);
            $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/upload/tmp/".$arrFiles["photo"]["name"]);
            $arFile["MODULE_ID"] = "main";
            $fid = CFile::SaveFile($arFile, "main");

            if (intval($fid)>0)
            {
                $arPhoto = CFile::MakeFileArray($fid);
                $fields = Array(
                    "PERSONAL_PHOTO" => $arPhoto,
                );
                $this->updUser($this->getID(), $fields);
                CFile::Delete($fid);
                unlink($_SERVER["DOCUMENT_ROOT"]."/upload/tmp/".$arrFiles["photo"]["name"]);
            }

        } else {
            return 0;
        }
    }

    public function getPathImg($fid)
    {
        return CFile::GetPath($fid);
    }

    public function getAvatar()
    {
        $data = $this->getByID($this->getID());
        return $this->getPathImg($data["PERSONAL_PHOTO"]);
    }

    public function getAvByID($ID)
    {
        $data = $this->getByID($ID);
        return $this->getPathImg($data["PERSONAL_PHOTO"]);
    }

    public function login($dataAuth)
    {
        if(empty($dataAuth["password"])) return;
        
        if (!is_object($USER)) $USER = new \CUser;
        $arAuthResult = $USER->Login($dataAuth["user_login"], $dataAuth["user_pass"], "Y");
        $APPLICATION->arAuthResult = $arAuthResult;

        if($arAuthResult != 1){
            array_push($this->errors, "Не верно введен старый пароль");
        }

        if ($arAuthResult == 1) {
            return 1;
        }
    }

    public function changePass($dataPass)
    {
        if(!empty($dataPass["password"]) && !empty($dataPass["confPass"]))
        {
            $user = new CUser;
            $fields = Array(
                "PASSWORD" => $dataPass["password"],
                "CONFIRM_PASSWORD" => $dataPass["confPass"]
            );
            $res = $user->Update($this->getID(), $fields);

            if($res){
                array_push($this->msg, "Пароль успешно изменен");
            } else {
                array_push($this->errors, $user->LAST_ERROR);
            }
        }

    }

    public function getLast()
    {
        $result = \Bitrix\Main\UserTable::getList(array(
            'select' => array('ID','LOGIN'),
            'order' => array('LAST_LOGIN'=>'DESC'),
            'limit' => 3
        ));

        while ($arUser = $result->fetch()) {
            print_r($arUser);
        }
    }

    public function translit($string)
    {
        // Транслитерация строк.
        if(!function_exists("rus2translit"))
        {
            function rus2translit($string) {
                $converter = array(
                    'а' => 'a',   'б' => 'b',   'в' => 'v',
                    'г' => 'g',   'д' => 'd',   'е' => 'e',
                    'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
                    'и' => 'i',   'й' => 'y',   'к' => 'k',
                    'л' => 'l',   'м' => 'm',   'н' => 'n',
                    'о' => 'o',   'п' => 'p',   'р' => 'r',
                    'с' => 's',   'т' => 't',   'у' => 'u',
                    'ф' => 'f',   'х' => 'h',   'ц' => 'c',
                    'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
                    'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
                    'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

                    'А' => 'A',   'Б' => 'B',   'В' => 'V',
                    'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
                    'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
                    'И' => 'I',   'Й' => 'Y',   'К' => 'K',
                    'Л' => 'L',   'М' => 'M',   'Н' => 'N',
                    'О' => 'O',   'П' => 'P',   'Р' => 'R',
                    'С' => 'S',   'Т' => 'T',   'У' => 'U',
                    'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
                    'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
                    'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
                    'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
                );
                return strtr($string, $converter);
            }
        }

        return rus2translit($string);
    }
}