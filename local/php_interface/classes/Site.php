<?php

class Site {

    public static function getName() {

        $rsSites = CSite::GetByID(SITE_ID);
        $arSite = $rsSites->Fetch();
        return $arSite["NAME"];
    }
}