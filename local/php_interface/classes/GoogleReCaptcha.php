<?php

class GoogleReCaptcha

{

    public static function getPublicKey() { return '6LdLz-AUAAAAAEOwOxVzzkB1kFirqNli5eSuwWoA';}

    public static function getSecretKey() { return '6LdLz-AUAAAAAIQnsY1jraD4TRBjyS6kMd5f4O0G';}

    public static function checkClientResponse(){

        $context = \Bitrix\Main\Application::getInstance()->getContext();

        $request = $context->getRequest();

        $captchaResponse = $request->getPost("g-recaptcha-response");

        if($captchaResponse) {

            $url = 'https://www.google.com/recaptcha/api/siteverify';

            $data = array(
                'secret' => static::getSecretKey(),
                'response' => $captchaResponse
            );

            $httpClient = new HttpClient();

            $response = $httpClient->post($url,$data);

            if($response){
                $response = \Bitrix\Main\Web\Json::decode($response,true);
            }

            if(!$response['success']) {

                return $response['error-codes'];

            }

            return array();

        }

        return array('no captcha response');
    }

}