<?php

class checkAuth {

    private $result;
    public $mesStatus;

    public function login($dataAuth) {

        global $USER;
        if (!is_object($USER)) $USER = new \CUser;
        $arAuthResult = $USER->Login($dataAuth["user_login"], $dataAuth["user_pass"], "Y");
        $APPLICATION->arAuthResult = $arAuthResult;

        if ($arAuthResult == 1) {
            $this->result = [ "TYPE"=>"OK", "MESSAGE"=>"Успешная авторизация" ];
            $this->mesStatus = true;
//            return;
            LocalRedirect("/");
        }

        if (empty($dataAuth["user_login"]) && empty($dataAuth["user_pass"]))
        {
            $this->result["MESSAGE"][] = "Ошибка! Вы забыли заполнить обязательные поля!";
            $this->mesStatus = false;
        }

        $res = Bitrix\Main\UserTable::getList(Array(
            "select"=>Array("ID", "LOGIN"),
            "filter"=>Array("=LOGIN" => $dataAuth["user_login"]),
        ));

        while ($arUser = $res->fetch()) {

            if ($arUser['ID']) {
                $this->result["MESSAGE"][] = "Неправильный логин или неправильный пароль";
                $this->mesStatus = false;
                return;
            }
        }

        $this->result["MESSAGE"][] = "Пользователь с данным логином или паролем не зарегистрирован. 
            <br>Пройдите регистрацию.";
        $this->mesStatus = false;
    }

    public function register($dataReg) {

        $this->mesStatus = true;

        if (empty($dataReg["user_login"]) && empty($dataReg["user_pass"]) )
        {
            $this->result["MESSAGE"][] = "Вы не заполнили обязательные поля!";
            $this->mesStatus = false;
        }

        if ( $dataReg["user_pass"] != $dataReg["conf_pass"])
        {
            $this->result["MESSAGE"][] = "Пароль подтвержения не совпадает";
            $this->mesStatus = false;
        }

        //Капча
        $secret = "6Ld5rOsUAAAAAEpMoOtz_N87uxOTQnDKN69N01nU";
        $response = null;
        $reCaptcha = new ReCaptcha($secret);

        if ($dataReg["recaptcha"]) {
            $response = $reCaptcha->verifyResponse(
                $_SERVER["REMOTE_ADDR"],
                $dataReg["recaptcha"]
            );
        }

        if ($response != null && $response->success) {
            $this->mesStatus = true;
        } else {
            $this->result["MESSAGE"][] = "Неверный код капча";
            $this->mesStatus = false;
        }

        if($this->mesStatus)
        {
            global $USER;
            $arResult = $USER->Register(
                $dataReg["user_login"],
                "",
                "",
                $dataReg["user_pass"],
                $dataReg["user_pass"],
                $dataReg["user_email"]
            );

            $user = new CUser;
            $fields = Array(
                "UF_BIRTHDAY" => $dataReg["user_date"],
            );
            $user->Update($USER->GetID(), $fields);
            $this->result = $arResult;
            if(isset($arResult["TYPE"]) && $arResult["TYPE"] == "OK")
            {
                LocalRedirect("/");
            }

        }
    }

    public function uniqUser($login) {

        $res = Bitrix\Main\UserTable::getList(Array(
            "select"=>Array("ID", "LOGIN"),
            "filter"=>Array("=LOGIN" => $login),
        ));

        while ($arUser = $res->fetch()) {

            if ($arUser['ID']) {
                return true;
            }
        }
        return false;
    }

    public function logout() {

        global $USER;
        $USER->Logout();
        LocalRedirect("/");
    }

    public function message() {
        $errors = "";

        if (!$this->mesStatus)
        {
            foreach ($this->result["MESSAGE"] as $error)
            {
                $errors .= ShowMessage($error);
            }
        } else {
            $errors = ShowMessage($this->result);
        }

        return $errors;

    }
}