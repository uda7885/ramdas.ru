<?php

class SocServ {

    public static function get() {

        return [
            "AUTH_SERVICES" => [
                "GoogleOAuth" => [
                    "ID" => "GoogleOAuth",
                    "CLASS" => "CSocServGoogleOAuth",
                    "NAME" => "Google",
                    "ICON" => "google",
                    "__sort" => 3,
                    "__active" => true,
                    "FORM_HTML" => [
                        "ON_CLICK" => "onclick=\"BX.util.popup('https://accounts.google.com/o/oauth2/auth?client_id=812474226206-13jiihjgq7e4in1svjft7hr3n545p2bp.apps.googleusercontent.com&amp;redirect_uri=http%3A%2F%2Framdas.ru%2Fbitrix%2Ftools%2Foauth%2Fgoogle.php&amp;scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&amp;response_type=code&amp;access_type=offline&amp;state=provider%3DGoogleOAuth%26site_id%3Ds1%26backurl%3D%252Flogin%252F%253Fcheck_key%253D6f9aa7f4950dbbc4dac85dfd63bd0903%26mode%3Dopener%26redirect_url%3D%252Flogin%252F', 580, 400)"
                    ],
                    "ONCLICK" => "BX.util.popup('https://accounts.google.com/o/oauth2/auth?client_id=812474226206-13jiihjgq7e4in1svjft7hr3n545p2bp.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Framdas.ru%2Fbitrix%2Ftools%2Foauth%2Fgoogle.php&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&response_type=code&access_type=offline&state=provider%3DGoogleOAuth%26site_id%3Ds1%26backurl%3D%252Flogin%252F%253Fcheck_key%253D6f9aa7f4950dbbc4dac85dfd63bd0903%26mode%3Dopener%26redirect_url%3D%252Flogin%252F', 580, 400)"
                ],

                "VKontakte" => [
                    "ID" => "VKontakte",
                    "CLASS" => "CSocServVKontakte",
                    "NAME" => "ВКонтакте",
                    "ICON" => "vkontakte",
                    "__sort" => 10,
                    "__active" => true,
                    "FORM_HTML" => [
                    "ON_CLICK" => "onclick=\"BX.util.popup('https://oauth.vk.com/authorize?client_id=7366542&amp;redirect_uri=http%3A%2F%2Framdas.ru%2Fbitrix%2Ftools%2Foauth%2Fvkontakte.php&amp;scope=friends,offline,email&amp;response_type=code&amp;state=site_id%3Ds1%26backurl%3D%252Flogin%252F%253Fcheck_key%253D6f9aa7f4950dbbc4dac85dfd63bd0903%26redirect_url%3D%252Flogin%252F', 660, 425)"
                    ],
                    "ONCLICK" => "BX.util.popup('https://oauth.vk.com/authorize?client_id=7366542&redirect_uri=http%3A%2F%2Framdas.ru%2Fbitrix%2Ftools%2Foauth%2Fvkontakte.php&scope=friends,offline,email&response_type=code&state=site_id%3Ds1%26backurl%3D%252Flogin%252F%253Fcheck_key%253D6f9aa7f4950dbbc4dac85dfd63bd0903%26redirect_url%3D%252Flogin%252F', 660, 425)"
                ],

                "Facebook" => [
                    "ID" => "Facebook",
                    "CLASS" => "CSocServFacebook",
                    "NAME" => "Facebook",
                    "ICON" => "facebook",
                    "__sort" => 13,
                    "__active" => true,
                    "FORM_HTML" => [
                    "ON_CLICK" => "onclick=\"BX.util.popup('https://www.facebook.com/dialog/oauth?client_id=219596976127716&amp;redirect_uri=http%3A%2F%2Framdas.ru%2Fbitrix%2Ftools%2Foauth%2Ffacebook.php&amp;scope=email,user_friends&amp;display=popup&amp;state=site_id%3Ds1%26backurl%3D%252Flogin%252F%253Fcheck_key%253D6f9aa7f4950dbbc4dac85dfd63bd0903%26redirect_url%3D%252Flogin%252F', 580, 400)"
                    ],
                    "ONCLICK" => "BX.util.popup('https://www.facebook.com/dialog/oauth?client_id=219596976127716&redirect_uri=http%3A%2F%2Framdas.ru%2Fbitrix%2Ftools%2Foauth%2Ffacebook.php&scope=email,user_friends&display=popup&state=site_id%3Ds1%26backurl%3D%252Flogin%252F%253Fcheck_key%253D6f9aa7f4950dbbc4dac85dfd63bd0903%26redirect_url%3D%252Flogin%252F', 680, 600)"
                ]
            ]
        ];
    }
}