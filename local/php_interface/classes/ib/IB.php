<?php

class IB {

    const IBLOCK_ID = 4;
    public static $arrErrors;

    /**
     * @param $id
     * @return int
     * �������� �������� �� ID, ��������� ���������� �� ����� � ��
     */
    public static function getByID($id) {

        self::inclModule();

        $arFilter = Array("IBLOCK_ID"=>self::IBLOCK_ID, "ACTIVE"=>"Y", "ID"=>$id);
        $res = \CIBlockElement::GetList(Array(), $arFilter, false, Array(), Array("*", "PROPERTY_SALE_DISCOUNT"));

        while($obj = $res->GetNextElement())
        {
            return $obj->GetFields();
        }
    }

    public static function getAll($id = 0, $idElem = "", $prop = "*") {

        if ($id == 0) return;
        self::inclModule();

        $arFilter = Array("IBLOCK_ID"=>$id, "ACTIVE"=>"Y", "ID"=>$idElem);
        $res = \CIBlockElement::GetList(Array(), $arFilter, false, Array(), Array($prop));

        while($obj = $res->GetNextElement())
        {
            $properties[] = $obj->GetFields();
        }

        return $properties;
    }

    /**
     * @return array
     * ���������� �������������� �������� ��
     */
    public static function getProp() {

        \Bitrix\Main\Loader::IncludeModule('iblock');

        $rsProperty = \Bitrix\Iblock\PropertyTable::getList(array(
            'filter' => array('IBLOCK_ID'=>self::IBLOCK_ID,'ACTIVE'=>'Y')
        ));

        while($prop = $rsProperty->fetch()) {
            $arProperty[] = mb_strtolower($prop["CODE"]);
        }

        return $arProperty;
    }

    public static function addElem($data) {

        $el = self::inclModule();

        if( $ID = $el->Add($data) ){
            return $ID;
        } else {
            echo $el->LAST_ERROR;
            return self::$arrErrors[] = $el->LAST_ERROR;
        }
    }

    public static function updElem($id, $data) {

        return self::inclModule()->Update($id, $data);
    }

    public static function delElem($id) {

        self::validData($id);

        if( !empty(self::$arrErrors) ) return;

        return self::inclModule()->Delete($id);

    }

    /**
     * @param $id
     * ��������� ������ ����� ����������� � �����������
     */
    public static function validData($id) {

        if ( ! is_numeric($id) || empty($id)){
            self::$arrErrors[] = "ID is not correct.";
        };

        if ( empty(self::getByID($id))) {
            self::$arrErrors[] = "Element not found.";
        }
    }

    /**
     * @param int $id
     * @param $data
     * @return mixed
     * �������������� �������� ������.
     * �������������� ������� ������� �� � ������� ������� ���� ������
     */
    public static function formatData( $data, $id=0 ) {

        //������� ������� ��
        foreach ($data["fields"] as $key=>$val) {

            if (in_array($key, self::getProp()) ) {
                $props[$key] = $val;
            } else {
                $newData[mb_strtoupper($key)] = $val;
            }
        }


        foreach ($props as $key=>$propVal) {

            $newData["PROPERTY_VALUES"][mb_strtoupper($key)] = $propVal;
            \CIBlockElement::SetPropertyValuesEx($id, self::IBLOCK_ID, Array(mb_strtoupper($key)=>$propVal) );

            // ������� ������� c ����� ������
            $properties = \CIBlockProperty::GetList(Array(), Array("IBLOCK_ID"=>self::IBLOCK_ID, "CODE"=>$key));

            while ($prop = $properties->GetNext()){

                if( $prop["PROPERTY_TYPE"] == "L" ) {
                    $lists[$key] = $propVal;
                }
            }
        }

        //���������� �������� c ����� ������
        foreach ($lists as $key=>$list) {

            $property_enums = \CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>self::IBLOCK_ID, "CODE"=>$key));

            while($enum_fields = $property_enums->GetNext())
            {
                if ( $enum_fields["VALUE"] == $list){
                    \CIBlockElement::SetPropertyValuesEx($id, self::IBLOCK_ID, Array(mb_strtoupper($key)=>$enum_fields["ID"]) );
                    $newData["PROPERTY_VALUES"][mb_strtoupper($key)] = $enum_fields["ID"];
                }

                $arFields[] = $enum_fields["VALUE"];
            }

            //���������� ������ �������
            if( !in_array($list, $arFields)) {

                $ibpenum = new \CIBlockPropertyEnum;

                if($PropID = $ibpenum->Add(Array('PROPERTY_ID'=>2, 'VALUE'=>$list))){
                    \CIBlockElement::SetPropertyValuesEx($id, self::IBLOCK_ID, Array(mb_strtoupper($key)=>$PropID) );
                    $newData["PROPERTY_VALUES"][mb_strtoupper($key)] = $PropID;
                }
            }
        }


        $newData["IBLOCK_ID"] = self::IBLOCK_ID;
        if ($id != 0) { unset($newData["PROPERTY_VALUES"]); }

        return $newData;
    }

    /**
     * @return mixed
     * ���������� ������ ������
     */
    public static function getErrors() {

        return self::$arrErrors;
    }

    /**
     * @return \CIBlockElement
     * ���������� ������ � ������� ������
     */
    public static function inclModule() {

        if( \CModule::IncludeModule("iblock"));

        return new \CIBlockElement;

    }

}
?>