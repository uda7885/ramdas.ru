<?php
use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();
if($request->get('q') && $APPLICATION->GetCurPage() != "/search/")
{
	LocalRedirect("/search/?q=" . $request->get('q') );
}

include \Bitrix\Main\IO\Path::normalize( $_SERVER["DOCUMENT_ROOT"] . '/local/vendor/autoload.php');

// Autoloader
\Bitrix\Main\Loader::registerAutoLoadClasses(null, array(
    '\IB' => '/local/php_interface/classes/ib/IB.php',
    '\checkAuth' => '/local/php_interface/classes/checkAuth.php',
    '\GoogleReCaptcha' => '/local/php_interface/classes/GoogleReCaptcha.php',
    '\Site' => '/local/php_interface/classes/Site.php',
    '\ReCaptchaResponse' => '/local/php_interface/classes/recaptchalib.php',
    '\ReCaptcha' => '/local/php_interface/classes/recaptchalib.php',
    '\Users' => '/local/php_interface/classes/Users.php',
    '\Videos' => '/local/php_interface/classes/Videos.php',
    '\SocServ' => '/local/php_interface/classes/SocServ.php',
));

AddEventHandler("main", "OnBeforeUserLogin", Array("CUserEx", "OnBeforeUserLogin"));
AddEventHandler("main", "OnBeforeUserRegister", Array("CUserEx", "OnBeforeUserRegister"));
AddEventHandler("main", "OnBeforeUserRegister", Array("CUserEx", "OnBeforeUserUpdate"));

class CUserEx
{
    function OnBeforeUserLogin($arFields)
    {
        $filter = Array("EMAIL" => $arFields["LOGIN"]);
        $rsUsers = CUser::GetList(($by="LAST_NAME"), ($order="asc"), $filter);
        if($user = $rsUsers->GetNext())
            $arFields["LOGIN"] = $user["LOGIN"];
    }
    function OnBeforeUserRegister($arFields)
    {
        $arFields["LOGIN"] = $arFields["EMAIL"];
    }
}

AddEventHandler("main", "OnAfterUserRegister", Array("ClassAfterRegister", "OnAfterUserRegisterHandler"));
class ClassAfterRegister
{
    function OnAfterUserRegisterHandler($arUser)
    {
        global $DB;
        $request = Application::getInstance()->getContext()->getRequest();
        $users = new Users();
        $fields = Array(
            "NAME" => $request->getPost("name"),
            "PERSONAL_BIRTHDAY" => $DB->FormatDate($request->getPost("date"), "YYYY-MM-DD", "DD.MM.YYYY"),
            "UF_REG" => "Y",
        );
        $users->updUser($arUser["USER_ID"], $fields);
    }
}

