<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
IncludeModuleLangFile(__FILE__);
global $APPLICATION;

$MODULE = 'itrack.webmessenger';

$RIGHT = $APPLICATION->GetGroupRight("main");
if ($RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ITRACK_WEBMESSENGER_DOSTUP_ZAPRESEN"));

$aTabs = array(
				array("DIV" => "edit1", "TAB" => GetMessage("ITRACK_WEBMESSENGER_KONSULQTANT"), ""),
				);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$res = CGroup::GetList(); 
$arGroups = array(); 
while ($arGroup = $res->GetNext()){
	$arGroups[$arGroup['ID']] = $arGroup['NAME'].((strlen($arGroup['DESCRIPTION']))?' ('.$arGroup['DESCRIPTION'].')':''); 
}



if($_REQUEST['save'] && check_bitrix_sessid()) {
    COption::SetOptionInt($MODULE, 'supportGroup', array_key_exists(intval($_REQUEST['supportGroup']), $arGroups) ? intval($_REQUEST['supportGroup']) : 1);
}

$APPLICATION->SetTitle(GetMessage("ITRACK_WEBMESSENGER_NASTROYKI_MODULA_VEB"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?
$tabControl->Begin();
?>
<form method="POST" enctype="multipart/form-data" action="<?echo $APPLICATION->GetCurPage()?>?lang=<?echo LANG?>&mid=<?=$_GET["mid"]?>">
    <?=bitrix_sessid_post()?>
	<?	
	$tabControl->BeginNextTab();
	?>
		<tr>
			<td width="50%" valign="top"><?=GetMessage("ITRACK_WEBMESSENGER_GRUPPA_KONSULQTANTOV")?></td>
			<td  valign="top">
				<select name="supportGroup" style="width: 500px;">
					<?foreach($arGroups as $iGroupId => $arGroupName):?>
					<option value="<?=$iGroupId?>" <?if(COption::GetOptionInt($MODULE, 'supportGroup', 1) == $iGroupId):?> selected="selected"<?endif?>><?=$arGroupName?></option>
					<?endforeach?>
				</select>
			</td>
		</tr>
	<?
	$tabControl->Buttons(
	  array(
		"disabled" => ($RIGHT < "W"),
		"back_url" => $_REQUEST["back_url"],
	  )
	);
	$tabControl->End();
	?>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>