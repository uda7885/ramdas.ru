<?
IncludeModuleLangFile(__FILE__);
Class itrack_webmessenger extends CModule
{
	const MODULE_ID = 'itrack.webmessenger';
	var $MODULE_ID = 'itrack.webmessenger'; 
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("itrack.webmessenger_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("itrack.webmessenger_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("itrack.webmessenger_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("itrack.webmessenger_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{
		global $DB, $DBType;
		$DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/'.self::MODULE_ID.'/install/db/'.strtolower($DBType).'/install.sql');
		RegisterModuleDependences('main', 'OnProlog', self::MODULE_ID, 'CItrackWebmessengerBase', 'OnProlog');
		RegisterModuleDependences('main', 'OnAfterUserAuthorize', self::MODULE_ID, 'CItrackWebmessengerBase', 'OnAfterUserAuthorize');
		
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		global $DB, $DBType;
		if ($_REQUEST['savedata'] != 'Y') {
			$DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/'.self::MODULE_ID.'/install/db/'.strtolower($DBType).'/uninstall.sql');
		}
		UnRegisterModuleDependences('main','OnProlog', self::MODULE_ID, 'CItrackWebmessengerBase', 'OnProlog');
		UnRegisterModuleDependences('main', 'OnAfterUserAuthorize', self::MODULE_ID, 'CItrackWebmessengerBase', 'OnAfterUserAuthorize');
		return true;
	}

	function InstallEvents()
	{
		global $DB;

		$sIn = "'ITR_MESSENGER_NEW_MESSAGE', 'ITR_MESSENGER_UNANSW_MESSAGES'";
		$rs = $DB->Query("SELECT count(*) C FROM b_event_type WHERE EVENT_NAME IN (".$sIn.") ", false, "File: ".__FILE__."<br>Line: ".__LINE__);
		$ar = $rs->Fetch();
		if($ar["C"] <= 0)
		{
			include($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/'.self::MODULE_ID.'/install/events/set_events.php');
		}
		
		return true;
	}

	function UnInstallEvents()
	{
		global $DB;
		
		include_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/'.self::MODULE_ID.'/install/events/del_events.php');
		return true;
	}

	function InstallFiles($arParams = array())
	{
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/admin'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.' || $item == 'menu.php')
						continue;
					file_put_contents($file = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.self::MODULE_ID.'_'.$item,
					'<'.'? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/'.self::MODULE_ID.'/admin/'.$item.'");?'.'>');
				}
				closedir($dir);
			}
		}
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/'.self::MODULE_ID.'/install/public/', $_SERVER["DOCUMENT_ROOT"].'/');
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/'.self::MODULE_ID.'/install/components', $_SERVER["DOCUMENT_ROOT"].'/bitrix/components', True, True);

		//CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/install/js", $_SERVER["DOCUMENT_ROOT"]."/bitrix/js/", true, true);
		//CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/install/gadgets", $_SERVER["DOCUMENT_ROOT"]."/bitrix/gadgets", true, true);
		
		
		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/'.self::MODULE_ID.'/install/public', $_SERVER["DOCUMENT_ROOT"]);
		
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/admin'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.') continue;
					unlink($_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.self::MODULE_ID.'_'.$item);
				}
				closedir($dir);
			}
		}
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;
		
		if ($GLOBALS['APPLICATION']->GetGroupRight('main') < 'W') {
			return;
		}
		
		$this->InstallFiles();
		$this->InstallDB();
		$this->InstallEvents();
		RegisterModule(self::MODULE_ID);
		CAgent::AddAgent("CItrackWebmessenger::NewMessNotifyAgent();", self::MODULE_ID, "N", 1800);
	}

	function DoUninstall()
	{
		global $APPLICATION;
		
		if ($GLOBALS['APPLICATION']->GetGroupRight('main') < 'W') {
			return;
		}
		
		if ($_REQUEST['step'] < 2) {
			$this->ShowDataSaveForm();
		} elseif ($_REQUEST['step'] == 2) {
			$this->UnInstallEvents();
			$this->UnInstallDB();
			$this->UnInstallFiles();
			CAgent::RemoveAgent("CItrackWebmessenger::NewMessNotifyAgent();", self::MODULE_ID);
			UnRegisterModule(self::MODULE_ID);
		}

	}
	
	function ShowDataSaveForm()
	{
		global $APPLICATION, $USER, $adminPage, $adminMenu, $adminChain;

		$PathInstall = str_replace('\\', '/', __FILE__);
		$PathInstall = substr($PathInstall, 0, strlen($PathInstall)-strlen('/index.php'));
		IncludeModuleLangFile($PathInstall.'/install.php');

		$GLOBALS['APPLICATION']->SetTitle($this->MODULE_NAME);
		include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');
		?>
		<form action="<?= $GLOBALS['APPLICATION']->GetCurPage()?>" method="get">
			<?= bitrix_sessid_post()?>
			<input type="hidden" name="lang" value="<?=LANG?>" />
			<input type="hidden" name="id" value="<?= self::MODULE_ID?>" />
			<input type="hidden" name="uninstall" value="Y" />
			<input type="hidden" name="step" value="2" />
			<?CAdminMessage::ShowMessage("Внимание!<br />Модуль будет удален из системы")?>
			<p>Вы можете сохранить данные в таблицах базы данных:</p>
			<p><input type="checkbox" name="savedata" id="savedata" value="Y" checked="checked" /><label for="savedata">Сохранить таблицы</label><br /></p>
			<input type="submit" name="inst" value="Удалить модуль" />
		</form>
		<?
		include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');
		die();
	}
	
}
?>
