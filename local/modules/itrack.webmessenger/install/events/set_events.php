<?
$langs = CLanguage::GetList(($b=""), ($o=""));
while ($lang = $langs->Fetch())
{
	$lid = $lang["LID"];
	IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/'.self::MODULE_ID.'/install/events.php', $lid);

	
	$et = new CEventType;
	$et->Add(array(
		"LID" => $lid,
		"EVENT_NAME" => "ITR_MESSENGER_NEW_MESSAGE",
		"NAME" => GetMessage("ITR_MESSENGER_NEW_MESSAGE_NAME"),
		"DESCRIPTION" => GetMessage("ITR_MESSENGER_NEW_MESSAGE_DESC"),
	));

	$et = new CEventType;
	$et->Add(array(
		"LID" => $lid,
		"EVENT_NAME" => "ITR_MESSENGER_UNANSW_MESSAGES",
		"NAME" => GetMessage("ITR_MESSENGER_UNANSW_MESSAGES_NAME"),
		"DESCRIPTION" => GetMessage("ITR_MESSENGER_UNANSW_MESSAGES_DESC"),
	));

	$arSites = array();
	$sites = CSite::GetList(($b=""), ($o=""), Array("LANGUAGE_ID"=>$lid));
	while ($site = $sites->Fetch())
		$arSites[] = $site["LID"];

	if(count($arSites) > 0)
	{
		$emess = new CEventMessage;
		$emess->Add(array(
			"ACTIVE" => "Y",
			"EVENT_NAME" => "ITR_MESSENGER_NEW_MESSAGE",
			"LID" => $arSites,
			"EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL_TO#",
			"SUBJECT" => GetMessage("ITR_MESSENGER_NEW_MESSAGE_SUBJECT"),
			"MESSAGE" => GetMessage("ITR_MESSENGER_NEW_MESSAGE_MESSAGE"),
			"BODY_TYPE" => "text",
		));

		$emess = new CEventMessage;
		$emess->Add(array(
			"ACTIVE" => "Y",
			"EVENT_NAME" => "ITR_MESSENGER_UNANSW_MESSAGES",
			"LID" => $arSites,
			"EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL_TO#",
			"SUBJECT" => GetMessage("ITR_MESSENGER_UNANSW_MESSAGES_SUBJECT"),
			"MESSAGE" => GetMessage("ITR_MESSENGER_UNANSW_MESSAGES_MESSAGE"),
			"BODY_TYPE" => "text",
		));		
	}
}
?>