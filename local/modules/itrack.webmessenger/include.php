<?
global $DBType;
IncludeModuleLangFile(__FILE__);


CModule::AddAutoloadClasses(
	"itrack.webmessenger",
	array(
		"CItrackWebmessengerBase"	=>	"classes/general/webmessenger.php",
		"CItrackWebmessenger"		=>	"classes/".$DBType."/webmessenger.php"
	)
);

?>
