<?
Class CItrackWebmessengerBase
{
	
	function OnAfterUserAuthorize($arUser) {
		$oMessenger = new CItrackWebmessenger();
		$oMessenger->changeOwner();
	}
	
	function OnProlog()
	{
		global $APPLICATION;
		$APPLICATION->AddHeadString('<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>',true);
		$APPLICATION->AddHeadString('<script>var jQuery_1_9_1 = $.noConflict(true);</script>',true);
		$APPLICATION->AddHeadString('<script type="text/javascript" src="/bitrix/modules/itrack.webmessenger/public/js/jquery-migrate-1.2.1.js"></script>',true);
		$APPLICATION->AddHeadString('<script type="text/javascript" src="/bitrix/modules/itrack.webmessenger/public/js/jquery.scrollTo-min.js"></script>',true);
		$APPLICATION->AddHeadString('<script type="text/javascript" src="/bitrix/modules/itrack.webmessenger/public/js/messenger.js"></script>',true);
	}
	
}
?>