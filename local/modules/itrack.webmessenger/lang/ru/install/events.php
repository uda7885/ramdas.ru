<?
$MESS["ITR_MESSENGER_NEW_MESSAGE_NAME"] = "Новое сообщение в чат";
$MESS["ITR_MESSENGER_NEW_MESSAGE_DESC"] = "Для вас новое сообщение в чате";
$MESS["ITR_MESSENGER_UNANSW_MESSAGES_NAME"] = "Неотвеченное сообщение в чате";
$MESS["ITR_MESSENGER_UNANSW_MESSAGES_DESC"] = "Уведомление о неотвечанных сообщениях";
$MESS["ITR_MESSENGER_NEW_MESSAGE_SUBJECT"] = "Новое сообщение";
$MESS["ITR_MESSENGER_NEW_MESSAGE_MESSAGE"] = "Для вас новое сообщение в чате";
$MESS["ITR_MESSENGER_UNANSW_MESSAGES_SUBJECT"] = "неотвеченное сообщение";
$MESS["ITR_MESSENGER_UNANSW_MESSAGES_MESSAGE"] = "Вы уже давно не отвечали на сообщение";
?>