<?
IncludeModuleLangFile(__FILE__);
$MODULE_ID = "itrack.webmessenger";
if($APPLICATION->GetGroupRight("itrack.webmessenger")!="D")
{
	CModule::IncludeModule('itrack.webmessenger');
	$aMenu = array(
			"parent_menu" => "global_menu_services",
			"section" => $MODULE_ID,
			"sort" => 50,
			"text" => GetMessage("ITRACK_WEBMESSENGER_VEB_MESSENDJER"),
			"title" => '',
			"icon" => "",
			"page_icon" => "",
			"items_id" => $MODULE_ID."_items",
			"more_url" => array(),
			"items" => array()
		);

		if (file_exists($path = dirname(__FILE__)))
		{
			if ($dir = opendir($path))
			{
				$arFiles = array();

				while(false !== $item = readdir($dir))
				{
					if (in_array($item,array('.','..','menu.php')))
						continue;

					if (!file_exists($file = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.$MODULE_ID.'_'.$item))
						file_put_contents($file,'<'.'? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/'.$MODULE_ID.'/admin/'.$item.'");?'.'>');

					$arFiles[] = $item;
				}

				sort($arFiles);

				foreach($arFiles as $item)
				{
					$strLangFileKeyName = "FILE_".strtoupper($item);
					//die($strLangFileKeyName);
					$aMenu['items'][] = array(
						'text' => (strlen(GetMessage($strLangFileKeyName))>0) ? GetMessage($strLangFileKeyName) : $item,
						'url' => $MODULE_ID.'_'.$item,
						'module_id' => $MODULE_ID,
						"title" => "",
					);
				}
			}
		}
		

	return $aMenu;
}
return false;
?>
