<?
// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/itrack.webmessenger/include.php"); // инициализация модуля

// подключим языковой файл
IncludeModuleLangFile(__FILE__);

// получим права доступа текущего пользователя на модуль
$POST_RIGHT = $APPLICATION->GetGroupRight("itrack.webmessenger");
// если нет прав - отправим к форме авторизации с сообщением об ошибке
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$sTableID = "tbl_webmessenger"; // ID таблицы
$oSort = new CAdminSorting($sTableID, "ID", "desc"); // объект сортировки
$lAdmin = new CAdminList($sTableID, $oSort); // основной объект списка

// ******************************************************************** //
//                           ФИЛЬТР                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// проверку значений фильтра для удобства вынесем в отдельную функцию
function CheckFilter()
{
    global $FilterArr, $lAdmin;
    foreach ($FilterArr as $f) global $$f;

    // В данном случае проверять нечего.
    // В общем случае нужно проверять значения переменных $find_имя
    // и в случае возниконовения ошибки передавать ее обработчику
    // посредством $lAdmin->AddFilterError('текст_ошибки').

    return count($lAdmin->arFilterErrors)==0; // если ошибки есть, вернем false;
}
// *********************** /CheckFilter ******************************* //

// опишем элементы фильтра
$FilterArr = Array(
    "find",
    "find_id",
    "find_user_id",
    "find_guest_id",
    "find_last_online",
);

// инициализируем фильтр
$lAdmin->InitFilter($FilterArr);

// если все значения фильтра корректны, обработаем его
if (CheckFilter())
{
    // создадим массив фильтрации для выборки CRubric::GetList() на основе значений фильтра
    $arFilter = Array(
        "ID"    => ($find!="" && $find_type == "id"? $find:$find_id),
        "USER_ID"    => $find_lid,
        "GUEST_ID"  => $find_active,
        "LAST_ONLINE"  => $find_visible,
    );
}

$oMessenger = new CItrackWebmessenger();

$arRes = $oMessenger->getWebmessengerUsersAll($by, $order);

// преобразуем список в экземпляр класса CAdminResult
$rsData = new CAdminResult($arRes, $sTableID);

// аналогично CDBResult инициализируем постраничную навигацию.
$rsData->NavStart();

// отправим вывод переключателя страниц в основной объект $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("rub_nav")));

// ******************************************************************** //
//                ПОДГОТОВКА СПИСКА К ВЫВОДУ                            //
// ******************************************************************** //

$lAdmin->AddHeaders(array(
    array(  "id"    =>"id",
        "content"  =>GetMessage("TITLE_ID"),
        "sort"    =>"id",
        "align"    =>"right",
        "default"  =>true,
    ),
    array(  "id"    =>"user_id",
        "content"  =>GetMessage("TITLE_USER_ID"),
        "sort"    =>"user_id",
        "align"    =>"right",
        "default"  =>true,
    ),
    array(  "id"    =>"guest_id",
        "content"  =>GetMessage("TITLE_GUEST_ID"),
        "align"    =>"right",
        "sort"    =>"guest_id",
        "default"  =>true,
    ),
    array(  "id"    =>"last_online",
        "content"  =>GetMessage("TITLE_LAST_ONLINE"),
        "sort"    =>"last_online",
        "align"    =>"right",
        "default"  =>true,
    ),
    array(  "id"    =>"message",
        "content"  =>GetMessage("TITLE_MESSAGE"),
        "align"    =>"right",
        "default"  =>true,
    ),
));

while($arRes = $rsData->NavNext(true, "f_")):

    // создаем строку. результат - экземпляр класса CAdminListRow
    $row =& $lAdmin->AddRow($f_ID, $arRes);

    // сформируем контекстное меню
    $arActions = Array();

endwhile;

// альтернативный вывод
$lAdmin->CheckListMode();

// установим заголовок страницы
$APPLICATION->SetTitle(GetMessage("rub_title"));

// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// ******************************************************************** //
//                ВЫВОД ФИЛЬТРА                                         //
// ******************************************************************** //

// создадим объект фильтра
$oFilter = new CAdminFilter(
    $sTableID."_filter",
    array(
        "ID",
        GetMessage("rub_f_site"),
        GetMessage("rub_f_active"),
        GetMessage("rub_f_public"),
        GetMessage("rub_f_auto"),
    )
);

// выведем таблицу списка элементов
$lAdmin->DisplayList();
?>

<?
// завершение страницы
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>