<? use \Bitrix\Main\Config\Option; ?>
</main>
<footer class="footer_section">
    <div class="container">
        <div class="footer">
            <img src="<?= SITE_TEMPLATE_PATH ?>/img/ait_b.png" alt="" class="ait_b">
            <img src="<?= SITE_TEMPLATE_PATH ?>/img/ain_m_foo.png" alt="" class="ait_m">
            <img src="<?= SITE_TEMPLATE_PATH ?>/img/ain_s_foo.png" alt="" class="ait_s">
            <div class="wrapMenuFooter">
                
                <?$APPLICATION->IncludeComponent("bitrix:menu", "main", Array(
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                        0 => "",
                    ),
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                ),
                    false
                );?>
    
                <?$APPLICATION->IncludeComponent("bitrix:menu", "video", Array(
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "video",	// Тип меню для остальных уровней
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                        0 => "",
                    ),
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "ROOT_MENU_TYPE" => "video",	// Тип меню для первого уровня
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                ),
                    false
                );?>
                
            </div>

            <a href="/" class="l_footer">
                <img src="<?= SITE_TEMPLATE_PATH ?>/img/logo.png" alt="">
            </a>
            <p class="copyright">&copy; 2002-2019 ООО "Sheeva yoga". Все права защищены.</p>
            <div class="networks n_footer">
                <a href="<?= Option::get("grain.customsettings", "fb")?>" target="_blank" class="network_link">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a href="<?= Option::get("grain.customsettings", "vk")?>" target="_blank" class="network_link">
                    <i class="fab fa-vk"></i>
                </a>
                <a href="<?= Option::get("grain.customsettings", "insta")?>" target="_blank" class="network_link">
                    <i class="fab fa-instagram"></i>
                </a>
            </div>
        </div>
    </div>
</footer>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/feedback.js" type="text/javascript"></script>
</body>
</html>