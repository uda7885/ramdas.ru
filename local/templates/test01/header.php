<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ramdas</title>
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/bootstrap.min.css">
<!--    <link rel="stylesheet" href="--><?//= SITE_TEMPLATE_PATH ?><!--/css/jquery-ui.min.css">-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900&display=swap&subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/main.css">
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH ?>/img/favicon.png" type="image/png">
    <?
	use \Bitrix\Main\Config\Option;
	require_once $_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH."/parts/shiva.php";
	?>

    <?$APPLICATION->ShowMeta("keywords")?>
    <?$APPLICATION->ShowMeta("description")?>
    <title><?$APPLICATION->ShowTitle()?> | <?= Site::getName() ?></title>
    <? $APPLICATION->ShowCSS();?>
    <? $APPLICATION->ShowHead();?>
</head>
<body onload="init();" style="margin:0px;" >
<? $APPLICATION->ShowPanel();?>
<header class="header_section">
    <div class="container">
        <nav class="top_panel">
            <img src="<?= SITE_TEMPLATE_PATH ?>/img/ait_b.png" alt="" class="ait_b">
            <img src="<?= SITE_TEMPLATE_PATH ?>/img/ait_m.png" alt="" class="ait_m">
            <img src="<?= SITE_TEMPLATE_PATH ?>/img/ait_s.png" alt="" class="ait_s">
            <a href="/" class="logo">
                <img src="<?= SITE_TEMPLATE_PATH ?>/img/logo.png" alt="">
            </a>
            <button type="button" class="burgerBtn" data-toggle="modal" data-target="#exampleModal">
                <span></span>
                <span></span>
                <span></span>
            </button>

            <!-- Burger -->
            <div class="modal fade burgerMenu" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="btn-secondary" data-dismiss="modal">X</button>
                    <div class="modal-body">

                        <? if($USER->GetLogin()): ?>
                            <?php $user = new Users(); ?>
                            <a class="lkUserLink" href="/lk">
                                <img style="width: 100%" src="<?= $user->getAvatar() ?? SITE_TEMPLATE_PATH."/img/profileMini.png"; ?>" alt="">
                            </a>
                        <? else: ?>
                        <div class="top_buttons">
                            <a href="/login" class="auth btn">
                                Войти
                            </a>
            				<div class="networks">
								<a href="<?= Option::get("grain.customsettings", "fb")?>" target="_blank" class="network_link">
									<i class="fab fa-facebook-f"></i>
								</a>
								<a href="<?= Option::get("grain.customsettings", "vk")?>" target="_blank" class="network_link">
									<i class="fab fa-vk"></i>
								</a>
								<a href="<?= Option::get("grain.customsettings", "insta")?>" target="_blank" class="network_link">
									<i class="fab fa-instagram"></i>
								</a>
            				</div>
                        </div>
                        <? endif; ?>


                        <div>
                             <?$APPLICATION->IncludeComponent("bitrix:menu", "main", Array(
                                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                                "CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
                                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                                "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                                    0 => "",
                                ),
                                "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                                "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                                "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                            ),
                                false
                            );?>
                        </div>
                         <img src="<?= SITE_TEMPLATE_PATH ?>/img/corners_line.png" class="mx-auto d-block"  style="margin-top: 1.5em; width: 100%;">

                        <div>
                             <?$APPLICATION->IncludeComponent("bitrix:menu", "video", Array(
                                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                                    "CHILD_MENU_TYPE" => "video",	// Тип меню для остальных уровней
                                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                                    "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                                        0 => "",
                                    ),
                                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                                    "ROOT_MENU_TYPE" => "video",	// Тип меню для первого уровня
                                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                                ),
                                false
                            );?>
                        </div>

                    </div>
                </div>
              </div>
            </div>

            <div class="desktopMenu">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "main", Array(
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                        0 => "",
                    ),
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                ),
                    false
                );?>


                <? if($USER->GetLogin()): ?>
                    <?php $user = new Users(); ?>
                    <a class="lkUserLink" href="/lk">
                        <img style="width: 100%" src="<?= $user->getAvatar() ?? SITE_TEMPLATE_PATH."/img/profileMini.png"; ?>" alt="">
                    </a>
                <? else: ?>
                <div class="top_buttons">
                    <a href="/login" class="auth btn">
                        Войти
                    </a>
    				<div class="networks">
						<a href="<?= Option::get("grain.customsettings", "fb")?>" target="_blank" class="network_link">
							<i class="fab fa-facebook-f"></i>
						</a>
						<a href="<?= Option::get("grain.customsettings", "vk")?>" target="_blank" class="network_link">
							<i class="fab fa-vk"></i>
						</a>
						<a href="<?= Option::get("grain.customsettings", "insta")?>" target="_blank" class="network_link">
							<i class="fab fa-instagram"></i>
						</a>
    				</div>
                </div>
                <? endif; ?>
            </div>
        </nav>
    </div>
    <form class="search">
        <?$APPLICATION->IncludeComponent(
		"bitrix:catalog.search",
		"search",
		array(
			"ACTION_VARIABLE" => "action",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"BASKET_URL" => "/personal/basket.php",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "N",
			"CONVERT_CURRENCY" => "N",
			"DETAIL_URL" => "",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"DISPLAY_COMPARE" => "N",
			"DISPLAY_TOP_PAGER" => "N",
			"ELEMENT_SORT_FIELD" => "sort",
			"ELEMENT_SORT_FIELD2" => "id",
			"ELEMENT_SORT_ORDER" => "asc",
			"ELEMENT_SORT_ORDER2" => "desc",
			"HIDE_NOT_AVAILABLE" => "N",
			"HIDE_NOT_AVAILABLE_OFFERS" => "N",
			"IBLOCK_ID" => "4",
			"IBLOCK_TYPE" => "videos",
			"LINE_ELEMENT_COUNT" => "3",
			"NO_WORD_LOGIC" => "N",
			"OFFERS_LIMIT" => "0",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => ".default",
			"PAGER_TITLE" => "Товары",
			"PAGE_ELEMENT_COUNT" => "30",
			"PRICE_CODE" => array(
			),
			"PRICE_VAT_INCLUDE" => "Y",
			"PRODUCT_ID_VARIABLE" => "id",
			"PRODUCT_PROPERTIES" => array(
			),
			"PRODUCT_PROPS_VARIABLE" => "prop",
			"PRODUCT_QUANTITY_VARIABLE" => "quantity",
			"PROPERTY_CODE" => array(
				0 => "",
				1 => "",
			),
			"RESTART" => "N",
			"SECTION_ID_VARIABLE" => "SECTION_ID",
			"SECTION_URL" => "",
			"SHOW_PRICE_COUNT" => "1",
			"USE_LANGUAGE_GUESS" => "Y",
			"USE_PRICE_COUNT" => "N",
			"USE_PRODUCT_QUANTITY" => "N",
			"USE_SEARCH_RESULT_ORDER" => "N",
			"USE_TITLE_RANK" => "N",
			"COMPONENT_TEMPLATE" => "search"
		),
		false
	);?>
    </form>
</header>
<main class="main_<?= ($APPLICATION->GetCurPage() == "/")? "page_": ""; ?>content">

	
    

