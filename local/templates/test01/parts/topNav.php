<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="/"><?= Site::getName(); ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">Главная
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/about">О нас</a>
                </li>

                <? if($USER->GetLogin()): ?>
                <li class="nav-item">
                    <a class="nav-link" href="/stat">Статистика</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><?= $USER->GetLogin() ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logout">Выход</a>
                </li>
                <? else: ?>
                <li class="nav-item">
                    <a class="nav-link" href="/login">Вход</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/reg">Регистрация</a>
                </li>
                <? endif; ?>
            </ul>
        </div>
    </div>
</nav>