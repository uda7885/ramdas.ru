$(document).ready(function () {
    $(".btn_ajax").click(function () {
        var form = ('#feed');
        var data = $(form).serialize();
        event.preventDefault();
        $.ajax({
            url: '/ajax/feed.php',
            data: data,
            type: 'POST',
            datatype: "html"
        }).done(function (data) {
            // console.log(data);
            $(".formWrap").append("<p>Данные отправлены. Мы вам обязательно перезвоним.</p>");
        }).fail(function (data) {
            console.log('Error: ' + data);
        });
        $('#successMsg').text('Success!');
    });
});