$(document).ready(function () {
    $(".btn_search").click(function (e) {
        e.preventDefault();
        var path = $("[type=search]").val();
        document.location.href="/search/?q=" + path;
    });
});