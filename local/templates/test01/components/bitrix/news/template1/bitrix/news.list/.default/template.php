<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$user = new Users();
CJSCore::Init(array("jquery"));
?>

<!-- Button trigger modal -->
<div class="mb-5">
	<? if($USER->getLogin()): ?>
		<button type="button" class="btn btn-primary text-right" data-toggle="modal" data-target="#exampleModal1">
			Добавить отзыв
		</button>
		<div class="result alert alert-success" style="display: none">
			Отзыв добавлен
		</div>
	<? endif; ?>
</div>

<?foreach($arResult["ITEMS"] as $arItem): ?>
	<?
	$user = new Users();
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="feedback_card">
		
		<div class="feedback_card_top">
			<div>
			    <div class="feedback_img">
        			<img src="<?= !empty($user->getAvByID($arItem["DISPLAY_PROPERTIES"]["USER_ID"]["VALUE"]))? $user->getAvByID($arItem["DISPLAY_PROPERTIES"]["USER_ID"]["VALUE"]) : SITE_TEMPLATE_PATH . "/img/profileMini.png"; ?>" alt="">
        		</div>
				<h4><?= $user->getName($arItem["DISPLAY_PROPERTIES"]["USER_ID"]["VALUE"]);?></h4>
				
			</div>
			<span>Premium пользователь</span>
			<?= $arItem["DISPLAY_PROPERTIES"]["VALUE"];?>
		</div>
		<p class="feedback_card_body">
			<?= $arItem["DETAIL_TEXT"]; ?>
		</p>
	</div>
<? endforeach; ?>

<div class="pagination">
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:system.pagenavigation",
			"pagin",
			array(
				"NAV_RESULT" => $arResult["NAV_RESULT"],
				"0" => "pagin",
				"COMPONENT_TEMPLATE" => "pagin"
			),
			false
		);?>
	<?endif;?>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Добавить отзыв</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<textarea name="review" id="review" rows="5" cols="40"></textarea>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary addReview" data-dismiss="modal">Добавить</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div>

<script>
	$(".addReview").click(function (e) {

		var data = "userID=<?= $user->getID(); ?>&text="+$("#review").val();

		$.ajax({
			type: "POST",
			url: "/ajax/review.php",
			data: data,
			datatype: "html"
		}).done(function (data) {
			$(".result").show();
		}).fail(function (data) {
			console.log(data);
		});
	});
</script>