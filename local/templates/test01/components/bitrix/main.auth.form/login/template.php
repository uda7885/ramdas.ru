<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die(); ?>

<section class="main_section">
	<div class="container">
		<div class="block_main pb-5">
			<div class="reg_title">
				Войти
			</div>
			<form class="form reg_form" method="post" action="">
				<input type="email" name="email" placeholder="Адрес эл. почты" required>
				<input type="password" name="password" placeholder="Пароль" required>
				<button class="btn">Войти</button>
			</form>

			<h2 class="reg_network">
				Войти с помощью соц. сетей
			</h2>
			<div class="enter_network">
			<?if ($arResult['AUTH_SERVICES']): ?>
				<?$APPLICATION->IncludeComponent('bitrix:socserv.auth.form',
					'flat',
					array(
						'AUTH_SERVICES' => $arResult['AUTH_SERVICES'],
						'AUTH_URL' => $arResult['CURR_URI']
					),
					$component,
					array('HIDE_ICONS' => 'Y')
				);
				?>
				<hr class="bxe-light">
			<?endif?>
			</div>
			
			<div class="text-center" style="margin-top: 20px">
				<a href="/register">или <br />зарегистрироваться</a>
			</div>
			<div class="text-center" style="margin-top: 20px">
				<a href="/restore_password/">Восстановить пароль</a>
			</div>
		</div>
	</div>
</section>

