<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<ul class="sub_menu d-flex">
<?
	foreach($arResult as $arItem):
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
			continue;
	?>
		<?if($arItem["SELECTED"]):?>
			<li class="menu_item">
				<a href="<?=$arItem["LINK"]?>" class="menu_link active"><?=$arItem["TEXT"]?></a>
			</li>
		<?else:?>
			<li class="menu_item">
				<a href="<?=$arItem["LINK"]?>" class="menu_link"><?=$arItem["TEXT"]?></a>
			</li>
	<?endif?>
	
<?endforeach?>

</ul>
<?endif?>