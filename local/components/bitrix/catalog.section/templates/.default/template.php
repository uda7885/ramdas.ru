<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$video = new Videos();

foreach ($arResult["ELEMENTS"] as $key=>$ID)
{
	$arResult["SEARCH_ITEMS"][] = $video->getVideo($ID);
}

//dump($arResult);
?>
<style>
	.search-page {display:none}
</style>
<!--<section class="sheeva_section">-->
<!--	<div class="container">-->
<!--		<div class="sheeva_block">-->
<!--			<canvas id="canvas" width="940" height="940" style="display: block; background-color:rgba(204, 204, 204, 0.00)"></canvas>-->
<!--		</div>-->
<!--	</div>-->
<!--</section>-->
<section class="video_block">
	<div class="container">
		<div class="block_main block_video block_corners_line">
			<h1>
				Поиск
			</h1>
			<div class="block_content">
				<div class="video_block">
					<? foreach ($arResult["SEARCH_ITEMS"] as $key=>$item): ?>
						<div class="video_text">
							<div class="v-wrapper">
								<? if($item["ACCEPT"]): ?>
									<video controls width="315" controlsList="nodownload" disablePictureInPicture>
										<source src="<?= $item["PROPERTY_VIDEO_PATH"];?>" type="video/mp4">
									</video>
								<? else: ?>
									<a href="<?= $item["LINK"]?>">
										<img src="<?= SITE_TEMPLATE_PATH?>/img/<?= $item["ACCEPT_IMG"] ?>" alt="">
									</a>
								<? endif; ?>
							</div>
							<p>
								<a href="<?= $item["ACCEPT"]? "/detail/". $item["ID"] : $item["LINK"]; ?>">
									<?= $item["NAME"]?>
								</a>
							</p>
						</div>
					<? endforeach; ?>
				</div>
			</div>
			<div class="pagination">
				<?$APPLICATION->IncludeComponent(
					"bitrix:system.pagenavigation",
					"pagin",
					array(
						"NAV_RESULT" => $arResult["NAV_RESULT"],
						"0" => "pagin",
						"COMPONENT_TEMPLATE" => "pagin"
					),
					false
				);?>
			</div>
		</div>
	</div>
</section>



