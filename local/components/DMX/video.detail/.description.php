<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); $arComponentDescription = array(
    "NAME" => GetMessage("Видео детальная"),
    "DESCRIPTION" => GetMessage("Видео детальная"),
    "PATH" => array(
        "ID" => "Ramdas",
        "CHILD" => array(
            "ID" => "Detail",
            "NAME" => "Видео детальная",
            "DESCRIPTION" => GetMessage("Страница с видео детальная"),
        )
    ),
    "ICON" => "/images/icon.gif",
);
?>