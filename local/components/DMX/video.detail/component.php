<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();

$users = new Users();
$videos = new Videos();
$ID = (int)$request->get("videoID");


if (empty($videos->getVideo($ID))) { return; }
$arResult["VIDEO"] = $videos->getVideo($ID);
$arResult["COACH"] = $videos->listVideoTrener($arResult["VIDEO"]["CREATED_BY"]);
$arResult["VIDEO"]["COACH_NAME"] = $users->getName($arResult["VIDEO"]["CREATED_BY"]);

if (!$arResult["VIDEO"]["ACCEPT"]) { return; }

$views = $arResult["VIDEO"]["PROPERTY_VIEWS_VALUE"];
$res = CIBlockElement::SetPropertyValuesEx($arResult["VIDEO"]["ID"], 4, ["VIEWS" => ++$views]);
$arResult["VIDEO"]["VIEWS"] = $views;

$this->IncludeComponentTemplate();
?>

