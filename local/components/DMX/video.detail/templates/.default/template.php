<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$item = $arResult["VIDEO"];
?>

<section class="video_block">
    <div class="container">
        <div class="block_main">
            <div class="block_content">
                <div class="video_block_wrap pt-5 mb-4">
                        <span class="data_download">
                            Загружено: <?= date('d.m.Y',strtotime($item["DATE_CREATE"])); ?>
                        </span>
                        <span class="available">
                            Доступно: <?= mb_strtolower($item["PROPERTY_TYPE_VALUE"]); ?>
                        </span>
                    <div class="big_video">
                        <? if($item["ACCEPT"]): ?>
                            <video controls width="952" controlsList="nodownload" disablePictureInPicture>
                                <source src="<?= $item["PROPERTY_VIDEO_PATH"];?>" type="video/mp4">
                            </video>
                        <? else: ?>
                            <a href="<?= $item["LINK"];?>">
                                <img src="<?= SITE_TEMPLATE_PATH?>/img/<?= $item["ACCEPT_IMG"] ?>" alt="">
                            </a>
                        <? endif; ?>
                    </div>

                    <span class="coach">
                        Тренер: <a href="/coach/<?= $item["CREATED_BY"]; ?>"> <?= $item["COACH_NAME"]; ?></a>
                    </span>
                    <span class="view">
                            Просмотров: <?= $item["VIEWS"]; ?>
                        </span>

                    
                </div>  
                <h2 class="title_bigVideo" style=" font-size: 20pt; text-align: start; margin-bottom: 15px">
                        <?= $item["NAME"]; ?>
                    </h2>
                
                <h3 class="text-center">
                    Все видео тренера
                </h3>

                <?$APPLICATION->IncludeComponent(
                    "DMX:video.coach",
                    "",
                    Array(
                        "ID_COACH" => $item["CREATED_BY"]
                    )
                );?>

                <a style="margin: 35px auto 0;     text-align: center;" href="/coach/<?= $item["CREATED_BY"]; ?>">
                    Перейти на страницу тренера
                </a>
            </div>
            
            <img  src="<?= SITE_TEMPLATE_PATH ?>/img/corners_line.png" class="mx-auto corner d-block" style="margin-top: 1.5em;">
        </div>
    </div>
</section>
