<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("Все видео"),
    "DESCRIPTION" => GetMessage("Все видео"),
    "PATH" => array(
        "ID" => "Ramdas",
        "CHILD" => array(
            "ID" => "allVideo",
            "NAME" => "Все видео",
            "DESCRIPTION" => GetMessage("Все видео"),
        )
    ),
    "ICON" => "/images/icon.gif",
);
?>