<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $USER;
if(!$USER->GetLogin()) {LocalRedirect("/");}

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();

$users = new Users();
$arResult["USER"] = [];

if (count($request) > 0 ) {
    
    $users->addImgProfile($_FILES);

    $dataAuth = [
        "user_login" => $USER->GetLogin(),
        "user_pass" => $request->getPost("oldPass"),
        "password" => $request->getPost("password")
    ];

    $dataPass = [
        "password" => $request->getPost("password"),
        "confPass" => $request->getPost("confPass")
    ];
    
    if($users->login($dataAuth)) {
        $users->changePass($dataPass);
    }

    $arFields = Array(
        "NAME" => $request->getPost("name"),
        "LAST_NAME" => $request->getPost("lastname"),
        "PERSONAL_NOTES" => $request->getPost("notes"),
        "PERSONAL_PHONE" => $request->getPost("phone"),
        "LID" => "ru",
        "ACTIVE" => "Y",
        "PERSONAL_BIRTHDAY" => $request->getPost("bday"),
        "PERSONAL_GENDER" => $request->getPost("gender"),
        "UF_INSTA" => $request->getPost("insta"),
    );

    $res = $users->updUser($USER->GetID(), $arFields);
    if($res){
        array_push($users->msg, "Данные успешно обновлены");
    } else {
        array_push($users->errors, $user->LAST_ERROR);
    }
}
    
$arResult["USER"] = $users->getByID($USER->GetID());
$arResult["USER"]["PERSONAL_PHOTO"] = $users->getPathImg($arResult["USER"]["PERSONAL_PHOTO"]);
$arResult["USER"]["MSG"] = $users->msg;
$arResult["USER"]["ERROR"] = $users->errors;
$arResult["USER"]["ISCOACH"] = $users->isCoach($USER->GetID());

//dump($arResult);
$this->IncludeComponentTemplate();
?>