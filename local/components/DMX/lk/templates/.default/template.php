<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$videos = new \Videos();
$user = $arResult["USER"];
CJSCore::Init(array("jquery","date"));
?>


<div class="alertWrap">
    <? if(!empty($arResult["USER"]["ERROR"])): ?>
        <?foreach ($arResult["USER"]["ERROR"] as $item): ?>
            <p class="alert alert-danger"><?= $item; ?></p>
        <?endforeach; ?>
    <? else: ?>
        <?foreach ($arResult["USER"]["MSG"] as $item): ?>
            <p class="alert alert-success"><?= $item; ?></p>
        <?endforeach; ?>
    <? endif; ?>
</div>

<section class="main_section cabinet_section">
    <div class="container">
        <div class="block_main pb-5">
            <div class="row columnLk">
                <div class=" col-md-6">
                    <form class="form reg_form lk_form mb-4" method="post" enctype="multipart/form-data">
                        <label for="name">Ваше Имя</label>
                        <input type="text" name="name" value="<?= $user["NAME"] ?? ""; ?>">
                        <label for="lastname">Фамилия</label>
                        <input type="text" name="lastname" value="<?= $user["LAST_NAME"] ?? ""; ?>">
                        <label for="bday">Дата рождения</label>
                        <input type="text" id="datepicker" class="bday" name="bday" onclick="BX.calendar({node: this, field: this, bTime: false});"
                               value="<?= (isset($user["PERSONAL_BIRTHDAY"]))? $user["PERSONAL_BIRTHDAY"]->format("d.m.Y") : ""; ?>">

                        <label for="gender">Пол</label>
                        <div class="gender_form">
                            <div class="gender custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="genderF" name="gender" value="F" <?= ($user["PERSONAL_GENDER"]=="F")? "checked" : "";?> >
                                <label id="checkBtn1" class="checkmark custom-control-label" for="genderF">Женский</label>
                            </div>

                            <div class="gender custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="genderM" name="gender" value="M" <?= ($user["PERSONAL_GENDER"]=="M")? "checked" : "";?> >
                                <label id="checkBtn2" class="checkmark custom-control-label" for="genderM">Мужской</label>
                            </div>
                        </div>
                </div>
                <div class="col-md-6 text-right imageUserLk">
                   <div id="lk_img">
                        <img src="<?= $user["PERSONAL_PHOTO"] ?? SITE_TEMPLATE_PATH."/img/profileBig.png"; ?>" alt="" class="w-100">
                   </div>
                    <div class="custom-file w-75">
                        <label class="custom-file-label" for="photo">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/img/subtract.png" >
                            Сменить фотографию
                            <input type="file" class="custom-file-input" id="photo" name="photo" accept="image/*">
                        </label>
                    </div>
                </div>
            </div>       
                <img src="<?= SITE_TEMPLATE_PATH ?>/img/corners_line.png" class="mx-auto d-block corner">
            <div class="row">
                <h5 class="mt-5 col-12">Контакты</h5>
                <div class="form-inline lk_contacts col-12">
                    <div class="form-group" style="display: flex;">
                        <label for="exampleFormControlInput1">Адрес эл.почты</label>
                        <input type="mail" class="form-control" id="email" name="email" value="<?= $user["EMAIL"] ?? ""; ?>">
                    </div>
                    <div class="form-group" style="display: flex;">
                        <label for="exampleFormControlInput1">Телефон</label>
                        <input type="tel" id="phone" class="form-control" name="phone" value="<?= $user["PERSONAL_PHONE"]?>" pattern="+[0-9]{1}-[0-9]{3}-[0-9]{3}-[0-9]{4}">
                    </div>
                    <div class="form-group" style="display: flex;">
                        <label for="exampleFormControlInput1">Instagram</label>
                        <input type="text" class="form-control" id="insta" name="insta" value="<?= $user["UF_INSTA"] ?? ""; ?>">
                    </div>
                </div>

                <h5 class="mt-5 col-12">О себе</h5>
                <div class="form-group col-12">
                    <textarea name="notes" id="" cols="30" rows="10"><?= $user["PERSONAL_NOTES"]?></textarea>
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary mt-2 ">Сохранить</button>
                </div>
                
            </div>

            <img  src="<?= SITE_TEMPLATE_PATH ?>/img/corners_line.png" class="corner mx-auto d-block" style="margin-top: 1.5em;">

            <div class="row">
                <div class="col-12 upload" id="upload">

                    <? if($user["ISCOACH"]): ?>
                    <h5 class="mt-5">Загрузить видео</h5>
                    <div class="upload_block form upload_form">  
                        <form class="form reg_form mb-4" id="upload_form" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="userID" value="<?= $user["ID"]; ?>">
                            <input type="text" name="videoTitle" placeholder="Название видео">
                            <h5 class="mt-2">Категория</h5>
                            <?php foreach ($videos->types() as $key=>$item):?>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" <?= ($key==18)? "checked" : ""; ?> class="custom-control-input" accept="video/*" id="<?= $key; ?>" value="<?= $key; ?>" name="videoType" <?= ($key==1)? "checked" : ""; ?>>
                                    <label class="custom-control-label" for="<?= $key; ?>"><?= $item; ?></label>
                                </div>
                            <?php endforeach; ?>

                            <h5 class="mt-2">Раздел</h5>
                            <select class="form-control" name="videoRazd">
                                <option value="0">Выберите раздел</option>
                                <?php foreach ($videos->razd() as $key=>$item):?>
                                    <option value="<?= $key; ?>"><?= $item; ?></option>
                                <?php endforeach; ?>
                            </select>
                            

                            <div class="custom-file">
                                Выберите файл
                                <label class="upload_label" for="videoFile">Указать путь</label>
                                <input type="file" name="videoFile" id="videoFile" сlass="custom-file-input">
                            </div>
                            <button class="btn btn-primary mt-2 " type="button" value="Загрузить файл" onclick="uploadFile()">Загрузить файл</button>
                            <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                            <h3 id="status"></h3>
                            <p id="loaded_n_total"></p>
                        </form>
                    </div>

                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/corners_line.png" class="corner mx-auto d-block">
                    <?php endif; ?>

                 </div>   
            </div>
            <div class="row">
                <div class="col-12">
                    <h5 class="mt-5">Сменить пароль</h5>
                        <form class="upload_form changePass">
                            <div class="form-group w-25">
                                <input type="password" class="form-control" id="oldPass" name="oldPass" placeholder="Старый пароль">
                            </div>
                            <div class="form-group w-25">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Пароль">
                            </div>
                            <div class="form-group w-25">
                                <input type="password" class="form-control" id="confPass" name="confPass" placeholder="Повторить пароль">
                            </div>
                           <div class="exitBtnBlock">
                                <button type="submit" class="btn btn-primary">Сменить пароль</button>
                                <a href="/logout">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/exit.png">
                                    Выйти
                                </a>
                           </div>
                        </form>
                </div>
                <img src="<?= SITE_TEMPLATE_PATH ?>/img/corners_line.png" class="corner mx-auto d-block"  style="margin-top: 1.5em;">
            </div>
        </div>
    </div>
</section>

<script>
//    $(function(){
//        $("#datepicker").datepicker();
//    });

    function _(el) {
        return document.getElementById(el);
    }

    function uploadFile() {
        var file = _("videoFile").files[0];
        var userID = document.querySelector('[name="userID"]').value;
        var videoTitle = document.querySelector('[name="videoTitle"]').value;
        var videoType = document.querySelector('[name="videoType"]:checked').id;
        var videoRazd = document.querySelector('[name="videoRazd"]').value;

        var formdata = new FormData();
        formdata.append("videoFile", file);
        formdata.append("userID", userID);
        formdata.append("videoTitle", videoTitle);
        formdata.append("videoType", videoType);
        formdata.append("videoRazd", videoRazd);

        var ajax = new XMLHttpRequest();
        ajax.upload.addEventListener("progress", progressHandler, false);
        ajax.addEventListener("load", completeHandler, false);
        ajax.addEventListener("error", errorHandler, false);
        ajax.addEventListener("abort", abortHandler, false);
        ajax.open("POST", "/ajax/Upload.php");
        ajax.send(formdata);
    }

    function progressHandler(event) {
        var percent = (event.loaded / event.total) * 100;
        _("progressBar").value = Math.round(percent);
        _("status").innerHTML = Math.round(percent)
            + "% загружено... пожалуйста подождите <input type='submit' value='Отменить' onclick='cancelUpload()'>";
    }

    function cancelUpload() {
        location.href = '/lk/index.php#upload';
    }

    function completeHandler(event) {
        _("status").innerHTML = event.target.responseText;
        _("progressBar").value = 0;
    }

    function errorHandler(event) {
        _("status").innerHTML = "Upload Failed";
    }

    function abortHandler(event) {
        _("status").innerHTML = "Upload Aborted";
    }
</script>
