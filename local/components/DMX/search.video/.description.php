<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("Поиск видео"),
    "DESCRIPTION" => GetMessage("Поиск видео"),
    "PATH" => array(
        "ID" => "Ramdas",
        "CHILD" => array(
            "ID" => "searchVideo",
            "NAME" => "Поиск видео",
            "DESCRIPTION" => GetMessage("Поиск видео"),
        )
    ),
    "ICON" => "/images/icon.gif",
);
?>