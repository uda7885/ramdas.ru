<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();
$videos = new Videos();

$elements = $videos->searchVideo($request->get('q'));
$countOnPage = 9;
$nav = new CDBResult();
$nav->NavPageCount = ceil(count($elements) / $countOnPage);
$nav->NavPageNomer = $request->get('PAGEN_1');
$nav->NavNum = 1;
$nav->NavPageSize = $countOnPage;
$nav->NavRecordCount = count($elements);
$begin = $request->get('PAGEN_1') ?? 0;
$begin = ($begin == 0 || $begin == 1)? 0 : ($request->get('PAGEN_1')-1) * $countOnPage;

if(empty($elements)){
    echo '<div class="not_found">Сожалеем, по вашему запросу ничего не найдено.</div>';
    return;
}
$arResult["NAV"] = $nav;
$arResult["ITEMS"] = array_slice($elements, $begin, $countOnPage);

$this->IncludeComponentTemplate();

?>