<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<section class="video_block">
    <div class="container">
        <div class="block_main block_video block_corners_line">
            <div class="block_content">
                <div class="video_block">
                    <? foreach ($arResult["ITEMS"] as $key=>$item): ?>
                    <div class="video_text">
                        <div class="v-wrapper">
                            <? if($item["ACCEPT"]): ?>
                            <a href="/detail/<?= $item["ID"]?>" class="wrap_video">
                                <video  class="video"  controls width="315" controlsList="nodownload" disablePictureInPicture oncontextmenu="return false;">
                                    <source src="<?= $item["PROPERTY_VIDEO_PATH"];?>" type="video/mp4">
                                </video>
                                <img class="playpause" src="<?= SITE_TEMPLATE_PATH ?>/img/free_video.png" >
                            </a>
                            <p><a href="/detail/<?= $item["ID"]?>"><?= $item["NAME"]?></a></p>
                            <? else: ?>
                                <a href="<?= $item["LINK"] ?>">
                                    <img src="<?= SITE_TEMPLATE_PATH?>/img/<?= $item["ACCEPT_IMG"] ?>" alt="">
                                </a>
                                <p><a href="<?= $item["LINK"] ?>"><?= $item["NAME"]?></a></p>
                            <? endif; ?>
                        </div>
                    </div>
                    <? endforeach; ?>
                </div>
            </div>
            
            <div class="pagination">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:system.pagenavigation",
                    "pagin",
                    array(
                        "NAV_RESULT" => $arResult["NAV"],
                        "0" => "pagin",
                        "COMPONENT_TEMPLATE" => "pagin"
                    ),
                    false
                );?>
            </div>
        </div>
    </div>
</section>
