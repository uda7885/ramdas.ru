<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();

$users = new Users();
$videos = new Videos();
$ID = $request->get("coachID");

if(!$users->getbyID($ID)){ return; }

$arResult["COACH"] = $users->getbyID($ID);
$arResult["COACH"]["ISCOACH"] = $users->isCoach($ID);
$arResult["COACH"]["VIDEO"] = $videos->listVideoTrener($ID);

//dump($arResult);

$this->IncludeComponentTemplate();
?>