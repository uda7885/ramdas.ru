<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); $arComponentDescription = array(
    "NAME" => GetMessage("Тренеры"),
    "DESCRIPTION" => GetMessage("Тренеры"),
    "PATH" => array(
        "ID" => "Ramdas",
        "CHILD" => array(
            "ID" => "Coach",
            "NAME" => "Тренеры",
            "DESCRIPTION" => GetMessage("Страница тренера"),
        )
    ),
    "ICON" => "/images/icon.gif",
);
?>