<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$coach = $arResult["COACH"];
?>

<section class="video_block">
    <div class="container">
        <div class="block_main block_corners_line">
            <div class="block_content">
                <div class="pt-5">
                    <div class="coach_block">
                        <div class="coach_img">
                            <img src="<?= CFile::getPath($coach["PERSONAL_PHOTO"]); ?>" alt="">
                        </div>
                        <div class="coach_about">
                            <div class="coach_about_top">
                                <div class="coach_name">
                                    <?= $coach["NAME"]; ?>
                                </div>
                                    <span class="coach_city">
                                <?= $coach["PERSONAL_CITY"]; ?>
                            </span>
                            <span class="coach_position">
                                <? if($arResult["COACH"]["ISCOACH"]): ?>
                                    Тренер
                                <? endif; ?>
                            </span>
                                <div class="coach_quantityVideo">
                                    Видео: <span><?= count($arResult["COACH"]["VIDEO"]); ?></span>
                                </div>
                            </div>
                            <div class="coach_about_bottom">
                                <? if ($coach["EMAIL"]): ?>
                                <a href="mailto:<?= $coach["EMAIL"]; ?>" class="coach_about_link">
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/mail.png" >
                                    Написать письмо
                                </a>
                                <? endif; ?>

                                <? if ($coach["PERSONAL_PHONE"]): ?>
                                <a href="mailto:<?= $coach["PERSONAL_PHONE"]; ?>" class="coach_about_link">
                                       <img src="<?= SITE_TEMPLATE_PATH ?>/img/call.png" >
                                    Позвонить
                                </a>
                                <? endif; ?>

                                <? if ($coach["UF_INSTA"]): ?>
                                <a href="#" class="coach_about_link">
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/insta.png" >
                                    <?= $coach["UF_INSTA"]; ?>
                                </a>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <h3>
                    Обо мне
                </h3>
                <p><?= $coach["PERSONAL_NOTES"]?></p>
                
                <h3 class="text-center">
                    Все видео тренера
                </h3>
                <div class="video_block">
                    <?$APPLICATION->IncludeComponent(
                        "DMX:video.coach",
                        "",
                        Array(
                            "ID_COACH" => $coach["ID"]
                        )
                    );?>

                </div>
                <a style="margin: 35px auto 0; text-align: center;" href="/all-video/" >
                    Смотреть все видео
                </a>
            </div>
        </div>
    </div>
</section>
