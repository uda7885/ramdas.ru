<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); $arComponentDescription = array(
    "NAME" => GetMessage("ЛК"),
    "DESCRIPTION" => GetMessage("ЛК"),
    "PATH" => array(
        "ID" => "Ramdas",
        "CHILD" => array(
            "ID" => "LK",
            "NAME" => "ЛК",
            "DESCRIPTION" => GetMessage("Личный кабинет"),
        )
    ),
    "ICON" => "/images/icon.gif",
);
?>