<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();
$users = new Users();

if (count($request) > 0 ) {

    $email = $request->getPost("email");
    if ($users->restore($email)) {
        $arResult["USER"]["MSG"] = $users->msg;
    } else {
        $arResult["USER"]["ERROR"] = $users->errors;
    }
}

$this->IncludeComponentTemplate();
?>