<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$user = $arResult["USER"];
?>

<div class="alertWrap">
    <? if(!empty($arResult["USER"]["ERROR"])): ?>
        <?foreach ($arResult["USER"]["ERROR"] as $item): ?>
            <p class="alert alert-danger"><?= $item; ?></p>
        <?endforeach; ?>
    <? else: ?>
        <?foreach ($arResult["USER"]["MSG"] as $item): ?>
            <p class="alert alert-success"><?= $item; ?></p>
        <?endforeach; ?>
    <? endif; ?>
</div>

<section class="main_section cabinet_section">
    <div class="container">
        <div class="block_main pb-5">
            <h5>Восстановить пароль</h5>
            <form class="" method="post">
                <div class="form-group" style="display: flex;">
    <!--                <label for="exampleFormControlInput1">Адрес эл.почты</label>-->
                    <input type="email" class="form-control" id="email" name="email" placeholder="Введите адрес почты">
                </div>
                <div>
                    <button type="submit" class="btn btn-primary mt-2 ">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</section>

