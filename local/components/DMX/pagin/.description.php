<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); $arComponentDescription = array(
    "NAME" => GetMessage("Пагинация"),
    "DESCRIPTION" => GetMessage("Пагинация"),
    "PATH" => array(
        "ID" => "Ramdas",
        "CHILD" => array(
            "ID" => "Pagin",
            "NAME" => "Пагинация",
            "DESCRIPTION" => GetMessage("Пагинация"),
        )
    ),
    "ICON" => "/images/icon.gif",
);
?>