<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

    <div class="video_block">
        <? foreach ($arResult["ITEMS"] as $key=>$item): ?>
        <div class="video_text">
            <div class="v-wrapper">
                <? if($item["ACCEPT"]): ?>
                <a  href="/detail/<?= $item["ID"]?>"  class="wrap_video">
                    <video class="video" controls width="315" controlsList="nodownload" disablePictureInPicture>
                        <source src="<?= $item["PROPERTY_VIDEO_PATH"];?>" type="video/mp4">
                    </video>
                    <img class="playpause" src="<?= SITE_TEMPLATE_PATH ?>/img/free_video.png" >
                </a>
                <? else: ?>
                    <a href="<?= $item["LINK"]; ?>">
                        <img src="<?= SITE_TEMPLATE_PATH?>/img/<?= $item["ACCEPT_IMG"] ?>" alt="">
                    </a>
                <? endif; ?>
            </div>
            <p>
                <a href="<?= $item["ACCEPT"]? "/detail/". $item["ID"] : $item["LINK"]; ?>">
                    <?= $item["NAME"]?>
                </a>
            </p>
        </div>
        <? endforeach; ?>
    </div>

    <div class="pagination">
        <?$APPLICATION->IncludeComponent(
            "bitrix:system.pagenavigation",
            "pagin",
            array(
                "NAV_RESULT" => $arResult["NAV"],
                "0" => "pagin",
                "COMPONENT_TEMPLATE" => "pagin"
            ),
            false
        );?>
    </div>

