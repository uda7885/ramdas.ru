<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();

$videos = new Videos();
$coach = $request->get("coach") ?? 0;
$coach = ($coach == 0)? $arParams["ID_COACH"] : "";

$elements = $videos->listVideoTrener($coach);
$countOnPage = 9;
$nav = new CDBResult();
$nav->NavPageCount = ceil(count($elements) / $countOnPage);
$nav->NavPageNomer = $request->get('PAGEN_1');
$nav->NavNum = 1;
$nav->NavPageSize = $countOnPage;
$nav->NavRecordCount = count($elements);
$begin = $request->get('PAGEN_1') ?? 0;
$begin = ($begin == 0 || $begin == 1)? 0 : ($request->get('PAGEN_1')-1) * $countOnPage;

if($coach) {
    $arResult["NAV"] = $nav;
    $arResult["ITEMS"] = array_slice($elements, $begin, $countOnPage);
}

$this->IncludeComponentTemplate();
?>