<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("Видео тренера"),
    "DESCRIPTION" => GetMessage("Видео тренера"),
    "PATH" => array(
        "ID" => "Ramdas",
        "CHILD" => array(
            "ID" => "VideoCoach",
            "NAME" => "Видео тренера",
            "DESCRIPTION" => GetMessage("Страница с видео тренера"),
        )
    ),
    "ICON" => "/images/icon.gif",
);
?>