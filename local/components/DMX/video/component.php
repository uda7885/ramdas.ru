<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();

$users = new Users();
$videos = new Videos();
$razd = $request->get("razd");

$elements = $videos->listVideoRazd( $videos->razdID($razd));
if (empty($elements)){ return; }

$countOnPage = 9;
$nav = new CDBResult();
$nav->NavPageCount = ceil(count($elements) / $countOnPage);
$nav->NavPageNomer = $request->get('PAGEN_1');
$nav->NavNum = 1;
$nav->NavPageSize = $countOnPage;
$nav->NavRecordCount = count($elements);
$begin = $request->get('PAGEN_1') ?? 0;
$begin = ($begin == 0 || $begin == 1)? 0 : ($request->get('PAGEN_1')-1) * $countOnPage;


$arResult["NAME"] = $videos->razdName($razd);
if($arResult["NAME"]) {
    $arResult["NAV"] = $nav;
    $arResult["ITEMS"] = array_slice($elements, $begin, $countOnPage);
}

$arResult["FREE_VIDEO"] = $videos->getFreeVideo();

//dump($arResult);
$this->IncludeComponentTemplate();
?>