<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h1>
    <?= $arResult["NAME"]; ?>
</h1>
<div class="block_content">
    <div class="video_block">
        <? foreach ($arResult["FREE_VIDEO"] as $key=>$item): ?>
        <div class="video_text" style="margin: 0">
            <div class="v-wrapper">
                <a  href="/detail/<?= $item["ID"]?>"  class="wrap_video">
                    <video class="video" controls width="315" controlsList="nodownload" disablePictureInPicture>
                        <source src="<?= $item["PROPERTY_VIDEO_PATH"];?>" type="video/mp4">
                    </video>
                    <img class="playpause" src="<?= SITE_TEMPLATE_PATH ?>/img/free_video.png" >
                </a>
            </div>
            <p>
                <a href="/detail/<?= $item["ID"]?>"><?= $item["NAME"]?></a>
            </p>
        </div>
        <? endforeach; ?>
    </div>
</div>