<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("Видео"),
    "DESCRIPTION" => GetMessage("Видео"),
    "PATH" => array(
        "ID" => "Ramdas",
        "CHILD" => array(
            "ID" => "Video",
            "NAME" => "Видео",
            "DESCRIPTION" => GetMessage("Страница с видео"),
        )
    ),
    "ICON" => "/images/icon.gif",
);
?>