<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Главная");
?><section class="sheeva_section">
	<div class="container">
		<div class="sheeva_block">
			<canvas id="canvas" width="940" height="940" style="display: block; background-color:rgba(204, 204, 204, 0.00)"></canvas>
		</div>
	</div>
</section>
<section class="blockquote">
	<div class="container">
		<aside>
			<img src="<?= SITE_TEMPLATE_PATH ?>/img/wo.png" alt="">
			<blockquote>
				Добро пожаловать в мир йоги!<br><br>
				Йога открывает невероятный простор для самопознания, открывая возможность воспринимать мир во всей его полноте.<br><br>
				Наша Миссия - способствовать стремлению к саморазвитию в современных условиях информационного многообразия.

			</blockquote>
		</aside>
	</div>
</section>
<section class="main_section">
	<div class="container">
		<div class="formWrap">
			<form class="form form_main" id="feed" onclick="return false;">
				<p>
					Появились вопросы? Мы открыты для общения и будем рады на них ответить – просто заполните эту форму и мы обязательно вам перезвоним!
				</p>
				<div class="form_items">
					<input type="text" name="name"  placeholder="Ваше имя">
					<input type="phone" name="phone"  placeholder="Телефон">
					<input type="submit" class="btn btn_ajax" value="Оставить заявку">
				</div>
			</form>
		</div>
		<div class="block_main block_main_page">
			<div class="block_content">
				<?$APPLICATION->IncludeComponent(
				"DMX:video",
				"main",
				Array()
				);?>
				<a href="/all-video/" class="mt-3">посмотреть все видео</a>
				<p  class="mt-5">
					Йога – это не очередной комплекс упражнений, каких немало. Йога – это эффективная практика, способная качественно изменить все аспекты жизни человека. На нашем портале представлены лучшие видеоуроки, подходящие как для начинающих, так и для тех, кто практикует йогу не первый год.
				</p>
				<p>
					Оформите подписку, чтобы получить полный доступ к видеоархиву!
				</p>
			</div>
			<img src="<?= SITE_TEMPLATE_PATH ?>/img/corners_line.png" class="mx-auto  d-block mt-3">
		</div>
		<aside class="main_aside">
			<img src="<?= SITE_TEMPLATE_PATH ?>/img/vector.png" alt="" >
			<blockquote>
				<p>
					Практикуя йогу, человек получает уникальный шанс обрести равновесие и гармонию, что особенно важно в наши дни, когда вездесущий стресс оказывает мощное давление на все аспекты повседневной жизни человека.
				</p>
				<p>
					Современная медицина рассматривает йогу прежде всего как инструмент в борьбе со стрессом, а уже только потом – как систему эффективных физических нагрузок.
				</p>
				<p>
					Важное преимущество йоги – её доступность. Для занятий не требуется дорогостоящее оборудование, не обязательно и посещение зала. Вы можете заниматься дома, на природе, выйдя во время обеденного перерыва в ближайший сквер или парк…
				</p>
				<p>
					Начать практиковать можно в любом возрасте и с любым уровнем физической подготовки – вы всегда сможете выбрать программу, подходящую именно вам.
				</p>
				<p>
					А наш проект постарается вам в этом помочь. На сайте вы всегда найдете:

				</p>
				<p style="padding-left: 25px;">
					•	Актуальный и полезный контент, касающийся всех аспектов повседневной практики йоги
				</p>
				<p style="padding-left: 25px;">
					•	Обзоры популярных йога-туров – и возможность приобрести лучшие авторские туры со скидкой
				</p>
				<p style="padding-left: 25px;">
					•	Интервью с лучшими тренерами и видеоуроки
				</p>
				<p style="padding-left: 25px;">
					•	Эксклюзивные предложения для наших подписчиков: йога-семинары, индивидуальные программы от ведущих тренеров, литература и многое другое.
				</p>
				<p>
					Подпишитесь на новости, чтобы первым получать выгодные предложения и участвовать в программе лояльности.
				</p>
			</blockquote>
		</aside>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>