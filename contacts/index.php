<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Задайте вопрос");
use \Bitrix\Main\Config\Option;
?>
	<section class="main_section">
		<div class="container">
			<div class="block_main block_corners_top">
				<h1>
					Контакты
				</h1>
				<div class="address_block">
					<div class="map">
						<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A8fd2a3c12e8bc0e1ce13eabc35bb35fdef755344b0d240398467ac1cc844849c&amp;width=555&amp;height=305&amp;lang=ru_RU&amp;scroll=false"></script>
					</div>
					<p>
						Адрес:
                        <span>
                            <?= Option::get("grain.customsettings", "address")?>
							<br><br>
                        </span>
						Телефон:
                        <span>
                            <?= Option::get("grain.customsettings", "phone1")?><br>
							<?= Option::get("grain.customsettings", "phone2")?><br>
                        </span>
					</p>
				</div>
				<h2 class="contact_h2">
					Следите за нами в соцсетях
				</h2>
				<div class="contact_network">
					<div class="wrapper_link">
						<a href="<?= Option::get("grain.customsettings", "fb")?>" target="_blank" class="network_link">
							<i class="fab fa-facebook-f"></i>
						</a>
						<a href="<?= Option::get("grain.customsettings", "fb")?>" target="_blank">facebook.com\</a>
					</div>
					<div class="wrapper_link">
						<a href="<?= Option::get("grain.customsettings", "vk")?>" target="_blank" class="network_link">
							<i class="fab fa-vk"></i>
						</a>
						<a href="<?= Option::get("grain.customsettings", "vk")?>" target="_blank">vk.com\</a>
					</div>
					<div class="wrapper_link">
						<a href="<?= Option::get("grain.customsettings", "insta")?>" target="_blank" class="network_link">
							<i class="fab fa-instagram"></i>
						</a>
						<a href="<?= Option::get("grain.customsettings", "insta")?>" target="_blank">
							#sheeva
						</a>

					</div>
				</div>
				<div class="formWrap">
					<form class="form" id="feed" onclick="return false;">
						<p>
							Так же связаться с нами можно заполнив форму обратной связи. Мы обязательно перезвоним вам в ближайщее время.
						</p>
						<div class="form_items">
							<input type="text" name="name" placeholder="Ваше имя">
							<input type="phone" name="phone" placeholder="Телефон">
							<input type="submit" class="btn btn_ajax" value="Оставить заявку">
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>